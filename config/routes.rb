Rails.application.routes.draw do

 

  #-----------------------------------------------------------------------------------
  # DEVISE 
  #-----------------------------------------------------------------------------------
  devise_for :users, :controllers => {sessions: 'user/sessions', registrations: 'user/registrations', passwords: 'user/passwords' } 

  #namespace :api do
  #    namespace :v1 do
  #      devise_for :users
  #    end
  #end

  #namespace :api, defaults: {format: 'json'} do
  #  namespace :v1 do
  #    resources :users
  #    devise_for :users, path: '', path_names: {sign_in: "login", sign_out: "logout"},
  #                                    controllers: {omniauth_callbacks: "authentication", registrations: "base"}
  #  end
  #end

  #-----------------------------------------------------------------------------------
  # API
  #-----------------------------------------------------------------------------------
  #namespace :api, default: { format: :json } do
  #    resources :articles, only: [:index]
  #end
  namespace :api do
    namespace :v1 do
      # Articles...
      resources :articles, :defaults => { :format => 'json' }
      # Players...
      resources :players, :defaults => { :format => 'json' } # xml
      resources :player_seasons, :defaults => { :format => 'json' }
      resources :player_badges, :defaults => { :format => 'json' }
      # Teams...
      resources :teams, :defaults => { :format => 'json' } # xml
      resources :team_seasons, :defaults => { :format => 'json' }
      resources :home_teams, :defaults => { :format => 'json' }
      resources :away_teams, :defaults => { :format => 'json' }
      resources :team_badges, :defaults => { :format => 'json' }
      # Delegates...
      resources :delegates, :defaults => { :format => 'json' }
      # Match...
      resources :matches, :defaults => { :format => 'json' }
      resources :goals, :defaults => { :format => 'json' }
      resources :assists, :defaults => { :format => 'json' }
      resources :fouls, :defaults => { :format => 'json' }
      resources :yellow_cards, :defaults => { :format => 'json' }
      resources :red_cards, :defaults => { :format => 'json' }
      resources :goalkeeper_saves, :defaults => { :format => 'json' }
      resources :shoots, :defaults => { :format => 'json' }
      post 'new_home_shoot', to: 'shoots#new_home', :defaults => { :format => 'json' }
      post 'new_away_shoot', to: 'shoots#new_away', :defaults => { :format => 'json' }
      put 'change_match_started', to: 'matches#change_match_started', :defaults => { :format => 'json' }
      patch 'change_match_started', to: 'matches#change_match_started', :defaults => { :format => 'json' }
      put 'change_first_half_ended', to: 'matches#change_first_half_ended', :defaults => { :format => 'json' }
      patch 'change_first_half_ended', to: 'matches#change_first_half_ended', :defaults => { :format => 'json' }
      put 'change_second_half_started', to: 'matches#change_second_half_started', :defaults => { :format => 'json' }
      patch 'change_second_half_started', to: 'matches#change_second_half_started', :defaults => { :format => 'json' }
      put 'change_second_half_ended', to: 'matches#change_second_half_ended', :defaults => { :format => 'json' }
      patch 'change_second_half_ended', to: 'matches#change_second_half_ended', :defaults => { :format => 'json' }
      resources :match_players, :defaults => { :format => 'json' }
      post 'chooseplayers', to: 'match_players#chooseplayers', :defaults => { :format => 'json' }
      # Players rang list
      resources :players_rang_list, :defaults => { :format => 'json' }
      get 'players_rang_list', to: 'players_rang_list#index', :defaults => { :format => 'json'}
      get 'players_rang_list_4_1', to: 'players_rang_list#index4plus1', :defaults => { :format => 'json'}
      # Teams rang list
      resources :teams_rang_list, :defaults => { :format => 'json' }
      get 'teams_rang_list', to: 'teams_rang_list#index', :defaults => { :format => 'json' }
      get 'teams_rang_list_4_1', to: 'teams_rang_list#index4plus1', :defaults => { :format => 'json' }
      # League...
      resources :leagues, :defaults => { :format => 'json' }
    end   
  end

  #-----------------------------------------------------------------------------------
  # RAILS ADMIN
  #----------------------------------------------------------------------------------- 
  mount RailsAdmin::Engine => '/admins', as: 'rails_admin'

  #-----------------------------------------------------------------------------------
  # COMMONTATOR
  #----------------------------------------------------------------------------------- 
  mount Commontator::Engine => '/commontator'
  
  #-----------------------------------------------------------------------------------
  # Ruta za sistem za razmenu poruka 
  #-----------------------------------------------------------------------------------
   resources :conversations, only: [:index, :show, :destroy] do
    member do
      post :reply
      post :restore
      post :mark_as_read
    end
    collection do
      delete :empty_trash
    end
  end

  #-----------------------------------------------------------------------------------
  # MESSAGES
  #-----------------------------------------------------------------------------------
  resources :messages, only: [:new, :create]
  get 'messages/new'

  #-----------------------------------------------------------------------------------
  # CONTACT MESSAGES - CONTACT PAGE
  #-----------------------------------------------------------------------------------
  resources :contactmessages, only: [:new, :create]
  

  #-----------------------------------------------------------------------------------
  # HOME TEAMS
  #-----------------------------------------------------------------------------------
  resources :home_teams

  #-----------------------------------------------------------------------------------
  # AWAY TEAMS
  #-----------------------------------------------------------------------------------
  resources :away_teams

  #-----------------------------------------------------------------------------------
  # STADIONS
  #-----------------------------------------------------------------------------------
  resources :stadions

  #-----------------------------------------------------------------------------------
  # REFEREE
  #-----------------------------------------------------------------------------------
  resources :referees

  #-----------------------------------------------------------------------------------
  # LIVESCORE
  #-----------------------------------------------------------------------------------
  resources :livescores

  #-----------------------------------------------------------------------------------
  # MATCHES
  #-----------------------------------------------------------------------------------
  resources :matches do
    member do
      # Akcija za belezenje vremena kada je utakmica pocela
      # MATCH STARTED
      put 'change_match_started', to: 'matches#change_match_started'
      patch 'change_match_started', to: 'matches#change_match_started'

      # FIRST HALF MATCH ENDED
      # (+ potrebna logika na kraju prvog poluvremena)
      put 'change_first_half_ended', to: 'matches#change_first_half_ended'
      patch 'change_first_half_ended', to: 'matches#change_first_half_ended'

      # SECOND HALF MATCH STARTED
      put 'change_second_half_started', to: 'matches#change_second_half_started'
      patch 'change_second_half_started', to: 'matches#change_second_half_started'

      # MATCH ENDED
      # (+ dodati logiku na kraju utakmice)
      put 'change_second_half_ended', to: 'matches#change_second_half_ended'
      patch 'change_second_half_ended', to: 'matches#change_second_half_ended'
    end
  end

  #-----------------------------------------------------------------------------------
  # LEAGUES
  #-----------------------------------------------------------------------------------
  resources :leagues
  
  get 'four_plus_one_leagues', to: 'leagues#fourplusone'
  get 'five_plus_one_leagues', to: 'leagues#fiveplusone'
  #-----------------------------------------------------------------------------------
  # TEAM SEASONS
  #-----------------------------------------------------------------------------------
  resources :team_seasons

  #-----------------------------------------------------------------------------------
  # PLAYER SEASONS
  #-----------------------------------------------------------------------------------
  resources :player_seasons

  #-----------------------------------------------------------------------------------
  # STATS
  #-----------------------------------------------------------------------------------
  resources :stats

  #-----------------------------------------------------------------------------------
  # RESULTS
  #-----------------------------------------------------------------------------------
  resources :results

  #-----------------------------------------------------------------------------------
  # SEASONS
  #-----------------------------------------------------------------------------------
  resources :seasons

  #-----------------------------------------------------------------------------------
  # MATCH PLAYERS
  #-----------------------------------------------------------------------------------
  #resources :match_players do
     #collection do
       #post :chooseplayers
  #resources :match_players
  get 'new_home_players', to: 'match_players#new_home', via: :post
  get 'new_away_players', to: 'match_players#new_away', via: :post
  post 'chooseplayers', to: 'match_players#chooseplayers'

  #-----------------------------------------------------------------------------------
  # GOALS
  #-----------------------------------------------------------------------------------
  resources :goals
  get 'new_home_goal', to: 'goals#new_home', via: :post
  get 'new_away_goal', to: 'goals#new_away', via: :post

  #-----------------------------------------------------------------------------------
  # ASSISTS
  #-----------------------------------------------------------------------------------
  resources :assists
  get 'new_home_assist', to: 'assists#new_home', via: :post
  get 'new_away_assist', to: 'assists#new_away', via: :post

  #-----------------------------------------------------------------------------------
  # FOULS
  #-----------------------------------------------------------------------------------
  resources :fouls
  get 'new_home_foul', to: 'fouls#new_home', via: :post
  get 'new_away_foul', to: 'fouls#new_away', via: :post

  #-----------------------------------------------------------------------------------
  # YELLOW CARDS
  #-----------------------------------------------------------------------------------
  resources :yellow_cards
  get 'new_home_yellow_card', to: 'yellow_cards#new_home', via: :post
  get 'new_away_yellow_card', to: 'yellow_cards#new_away', via: :post

  #-----------------------------------------------------------------------------------
  # RED CARDS
  #-----------------------------------------------------------------------------------
  resources :red_cards
  get 'new_home_red_card', to: 'red_cards#new_home', via: :post
  get 'new_away_red_card', to: 'red_cards#new_away', via: :post

  #-----------------------------------------------------------------------------------
  # SHOOTS
  #-----------------------------------------------------------------------------------
  resources :shoots
  get 'new_home_shoot', to: 'shoots#new_home', via: :post
  get 'new_away_shoot', to: 'shoots#new_away', via: :post

  #-----------------------------------------------------------------------------------
  # GOALKEEPER SAVES
  #-----------------------------------------------------------------------------------
  resources :goalkeeper_saves
  get 'new_home_goalkeeper_save', to: 'goalkeeper_saves#new_home', via: :post
  get 'new_away_goalkeeper_save', to: 'goalkeeper_saves#new_away', via: :post

  #-----------------------------------------------------------------------------------
  # LEAGUE SEASONS
  #-----------------------------------------------------------------------------------
  get 'league_seasons/index'
  get 'league_seasons/new'
  get 'league_seasons/create'
  get 'league_seasons/edit'
  get 'league_seasons/update'
  get 'league_seasons/show'

  #-----------------------------------------------------------------------------------
  # LEAGUE PAGES
  #-----------------------------------------------------------------------------------
  resources :league_pages
  #get 'league_pages/index'
  #get 'league_pages/show'
  #get 'league_pages/update'

  #-----------------------------------------------------------------------------------
  # GOALKEEPER SAVES
  #-----------------------------------------------------------------------------------
  resources :goalkeeper_saves
  get 'new_home_goalkeeper_save', to: 'goalkeeper_saves#new_home', via: :post
  get 'new_away_goalkeeper_save', to: 'goalkeeper_saves#new_away', via: :post

  #-----------------------------------------------------------------------------------
  # RANKINGS
  #-----------------------------------------------------------------------------------
  get 'rankings/index'
  get 'rankings/show'
  get 'rankings/update'
  get 'rankings/teams'
  get 'rankings/players'

  #-----------------------------------------------------------------------------------
  # PLAYERS RANG LIST
  #-----------------------------------------------------------------------------------
  get 'players_rang_list', to: 'players_rang_list#index'
  get 'players_rang_list_4_1', to: 'players_rang_list#index4plus1'

  #-----------------------------------------------------------------------------------
  # TEAMS RANG LIST
  #-----------------------------------------------------------------------------------
  get 'teams_rang_list', to: 'teams_rang_list#index'
  get 'teams_rang_list_4_1', to: 'teams_rang_list#index4plus1'

  #-----------------------------------------------------------------------------------
  # TEAM BADGES
  #-----------------------------------------------------------------------------------
  resources :team_badges

  #-----------------------------------------------------------------------------------
  # TEAM PLAYERS
  #-----------------------------------------------------------------------------------
  resources :team_players

  #-----------------------------------------------------------------------------------
  # PLAYER BADGES
  #-----------------------------------------------------------------------------------
  resources :player_badges

  #-----------------------------------------------------------------------------------
  # OWN GOALS
  #-----------------------------------------------------------------------------------
  get 'own_goals/index'
  get 'own_goals/show'
  get 'own_goals/update'

  #-----------------------------------------------------------------------------------
  # COMMENTS
  #-----------------------------------------------------------------------------------
  get 'comments/index'
  get 'comments/show'
  get 'comments/update'

  #-----------------------------------------------------------------------------------
  # CALENDAR
  #-----------------------------------------------------------------------------------
  resources :calendar

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  ################################
  # 01. Welcome (root)
  ################################
  get "welcome/index"
  root :to => 'welcome#index'
  #get 'home', to: 'welcome#home'
  #get 'about', to: 'welcome#about'
  #get 'kontakt', to: 'welcome#contact'

  resources :welcome_pages
  get 'kontakt', to: 'welcome_pages#contact'
  #get 'vesti', to: 'welcome_pages#news'
  #get 'pravila', to: 'welcome_pages#rules'

  resources :tournaments
  
  resources :rules
  get 'rules51', to: 'rules#rules51'
  get 'rules41', to: 'rules#rules41'
  get 'izvod', to: 'rules#izvod'

  ################################
  # 02. Users
  ################################
  #get 'signup', to: 'users#new'
  resources :users, except: [:new, :create]

  ################################
  # 03. Admins
  ################################
  resources :admins
  get 'registruj_admina', to: 'admins#new'

  #get 'sve_utakmice', to: 'admins#matches'
  #et 'sve_vesti', to: 'admins#news'
  #get 'sve_lige', to: 'admins#leagues'
  #get 'svi_kupovi', to: 'admins#coupes'
  #get 'sve_ekipe', to: 'admins#teams'
  #get 'svi_igraci', to: 'admins#players'

  ################################
  # 04. Players
  ################################
  resources :players

  ################################
  # Teams (Register a new team)
  ################################
  get 'registruj_ekipu', to: 'teams#new'
  resources :teams
  get 'new_ten_players', to: 'teams#new_ten_players', via: :post
  get 'new_twelve_players', to: 'teams#new_twelve_players', via: :post
  get 'new_fourteen_players', to: 'teams#new_fourteen_players', via: :post
  get 'new_sixteen_players', to: 'teams#new_sixteen_players', via: :post

  ################################
  # 05. Delegates
  ################################
  get 'registruj_delegata', to: 'delegates#new'
  resources :delegates

  #-----------------------------------------------------------------------------------
  # 06. Articles
  #-----------------------------------------------------------------------------------
  resources :articles
  
  ################################
  # 07. Seasson
  ################################
  #get 'login', to: 'sessions#new'
  #post 'login', to: 'sessions#create'
  #delete 'logout', to: 'sessions#destroy'
  
end
