require File.expand_path('../boot', __FILE__)

require 'rails/all'

############################################
# Ova komanda je za sistem za razmenu poruka 
############################################

# Pick the frameworks you want:
require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
ENV['RAILS_ADMIN_THEME'] = 'rollincode'

module GTCFutsal
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.active_record.time_zone_aware_attributes = true

    # Devise custom failure
    config.autoload_paths += %W(#{config.root}/lib)

    # config.web_console.whitelisted_ips = '192.168.1.103'

    # Do not swallow errors in after_commit/after_rollback callbacks.
    # config.active_record.raise_in_transactional_callbacks = true

    # Rack::Cors
    config.middleware.insert_before 0, "Rack::Cors" do
        allow do
            origins '*'
            resource '*', :headers => :any, :methods => [ :get, :put, :post, :options, :delete]
        end
    end

    #config.action_dispatch.default_headers = {
    #    'Access-Control-Allow-Origin' => '*',
    #    'Access-Control-Request-Method' => %w{GET POST OPTIONS}.join(",")
    #  }
  end
end
