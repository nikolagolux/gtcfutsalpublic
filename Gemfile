source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.6'

###############################
# DB
###############################
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.3.13', '< 0.5'
gem 'seed_dump'

###############################
# Dokumentacija
###############################
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

###############################
# STYLESHEETS
###############################
# Use SCSS for stylesheets
gem 'bootstrap-sass', '~> 3.3.6'
gem 'sass-rails', '~> 5.0'
gem 'compass-rails'

#################################
# GEMOVI ZA SISTEM RAZMENE PORUKA
#################################
gem 'thin'
gem 'mailboxer'

###############################
# JAVASCRIPT
###############################
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

###############################
# IMAGES
###############################
gem "paperclip", "~> 5.0.0.beta1"
#group :development do
gem 'rails_real_favicon'
#end

###############################
# COMMENTS
###############################
gem 'commontator', '~> 4.11.1'
###############################
# SISTEM ZA GLASANJE
###############################
gem 'acts_as_votable', '~> 0.10.0'

###############################
# Autentifikacija
###############################
#DEVISE
gem 'devise'
gem 'rack-cors'
gem 'active_model_serializers'
# Use ActiveModel has_secure_password
#gem 'bcrypt', '~> 3.1.7'

###############################
# Administratorski panel (CMS)
###############################
gem 'rails_admin'#, git: 'https://github.com/sferik/rails_admin.git'
gem 'rails_admin_rollincode', '~> 1.0'

###############################
# Calendar
###############################
gem "simple_calendar", "~> 2.0"

###############################
# Will paginate
###############################
gem 'will_paginate', '3.0.7'
gem 'bootstrap-will_paginate', '0.0.10'

###############################
# DEBUG
###############################
group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
end

###############################
# Ostalo
###############################
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

###############################
# PUMA Web Server
###############################
gem 'puma'

#The twelve-factor gem, http://12factor.net/
#Raspitati se o ovom gemu
gem 'rails_12factor', group: :production

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
#gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
