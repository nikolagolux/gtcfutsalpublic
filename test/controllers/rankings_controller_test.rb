require 'test_helper'

class RankingsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get update" do
    get :update
    assert_response :success
  end

  test "should get teams" do
    get :teams
    assert_response :success
  end

  test "should get players" do
    get :players
    assert_response :success
  end

end
