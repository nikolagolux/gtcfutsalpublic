class HomeTeam < ActiveRecord::Base
	#############################################################
	# Ne smemo da imamo vise home_team-ova sa istim team_id
	#############################################################
	validates_uniqueness_of :team_id
	#############################################################
	has_one :match
	belongs_to :team

	def name
		if self.team != nil
			"#{self.team.name}"
		end
	end
end
