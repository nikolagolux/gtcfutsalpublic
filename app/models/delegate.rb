class Delegate < ActiveRecord::Base
  has_one :user
  has_one :match
  accepts_nested_attributes_for :user

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => lambda { |avatar| avatar.instance.set_default_url}
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  #validates :JMBG, presence:true

  def set_default_url
  ActionController::Base.helpers.asset_path('missing.png')
  end

  def avatar_url
    "http://balf.rs#{self.avatar.url(:original)}"
  end

  def title
  	"#{name} #{surname}"
  end

  def full_name
    "#{name} #{surname}"
  end
end


