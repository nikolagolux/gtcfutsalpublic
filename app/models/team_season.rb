class TeamSeason < ActiveRecord::Base
	#############################################################
	# Ne smemo da imamo vise player sezona sa istom kombinacijom
	# (player_id, season_id)
	#############################################################
	validates_uniqueness_of :team_id, :scope => :season_id
	#############################################################

	belongs_to :team
	belongs_to :season

	#REST Api

	def team_name
		self.team.name
	end

	def league_name
		if self.team != nil
	      if self.team.league != nil
	        self.team.league.name
	      end
	    end
	end
end
