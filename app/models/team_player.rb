class TeamPlayer < ActiveRecord::Base
	belongs_to :team

  # Dva reda ispod dolaze uz Paperclip gem
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>", :logo => "40x40>"}, :default_url => lambda { |avatar| avatar.instance.set_default_url}
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates :name, presence:true

  def set_default_url
    ActionController::Base.helpers.asset_path('default_team.png')
  end

  def name
  	self.team.name
  end
end
