class League < ActiveRecord::Base
	has_many :teams
	has_many :matches
	has_many :articles
	
	############################################
	# Ova komanda je za sistem za postavljanje
	# komentara
	############################################
	
	acts_as_commontable
end
