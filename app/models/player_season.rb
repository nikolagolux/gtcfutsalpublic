class PlayerSeason < ActiveRecord::Base
  #############################################################
  # Ne smemo da imamo vise player sezona sa istom kombinacijom
  # (player_id, season_id)
  #############################################################
  validates_uniqueness_of :player_id, :scope => :season_id
  #############################################################

  belongs_to :player

  def name
  	"#{self.player.name}"
  end

  def surname
  	"#{self.player.surname}"
  end

  def full_name
    "#{self.player.name} #{self.player.surname}"
  end

  def team_name
    if self.player.team != nil
  	  self.player.team.name
    end
  end


end