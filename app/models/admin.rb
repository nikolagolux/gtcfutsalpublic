class Admin < ActiveRecord::Base
  has_one :user
  accepts_nested_attributes_for :user

  def full_name
    "#{name} #{surname}"
  end
end