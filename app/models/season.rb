class Season < ActiveRecord::Base
	# Pri kreiranju sezone, kreiraju se nove:
	# 1) Player sezone za sve igrace
	# 1) Team sezone za sve timove
	# 1) League sezone za sve lige
	before_save  :change_token,
				 :change_delegate_token,
				 :change_player_token
	after_create :create_new_player_seasons,
				 :create_new_player_badges,
				 :create_new_team_seasons,
				 :create_new_team_badges,
				 :create_new_league_seasons

	has_many :team_seasons
	has_many :matches
	private
	def change_token
		self.registration_token = rand(1051..9958)
	end

	def change_delegate_token
		self.delegate_registration_token = rand(1051..9958)
	end

	def change_player_token
		self.player_registration_token = rand(1051..9958)
	end

	def create_new_player_seasons
		#Player.update(:season_id)
		@players = Player.all
		@players.each do |player|
			@player_season = PlayerSeason.find_by(:player_id => player.id, :season_id => Season.last(2).first.id)
			if @player_season
				@experience = @player_season.expirience
				@level = @player_season.level
				# player.player_season.update(:player_id => player.id + (10000*self.id))
				@player_season.update(:player_id => player.id)
				PlayerSeason.create(:expirience => @experience, :level => @level, :season_id => self.id, :player_id => player.id)
			else
				PlayerSeason.create(:season_id => self.id, :player_id => player.id)
			end
		end
	end

	def create_new_player_badges
		#Player.update(:season_id)
		@players = Player.all
		@players.each do |player|
			PlayerBadge.create(:season_id => self.id, :player_id => player.id)
		end
	end

	def create_new_team_seasons
		#Player.update(:season_id)
		@teams = Team.all
		@teams.each do |team|
			@team_season = TeamSeason.find_by(:team_id => team.id, :season_id => Season.last(2).first.id)
			if @team_season
				@experience = @team_season.expirience
				@level = @team_season.level
				@team_season.update(:team_id => team.id)
				TeamSeason.create(:expirience => @experience, :level => @level, :season_id => self.id, :team_id => team.id)
			end
			TeamSeason.create(:season_id => self.id ,:team_id => team.id)
		end
	end

	def create_new_team_badges
		#Player.update(:season_id)
		@teams = Team.all
		@teams.each do |team|
			TeamBadge.create(:team_id => team.id, :season_id => self.id)
		end
	end

	def create_new_league_seasons
		#Player.update(:season_id)
		@leagues = League.all
		@leagues.each do |league|
			LeagueSeason.create(:season_id => self.id ,:league_id => league.id)
		end
	end
end
