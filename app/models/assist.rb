class Assist < ActiveRecord::Base
	after_create :prva_asistencija,
				 :increment_player_season_assist,
				 :increment_current_match_assist,
				 :increment_assists_in_chain,
				 :increment_team_season_assists

	belongs_to :match
	belongs_to :player_season

	private

	def prva_asistencija
	 	@player_season = PlayerSeason.find_by(:id => self.player_season.id)

	 	#Logika za prvu asistenciju
	 	if @player_season[:assists] == 0
	 		@player_badge = PlayerBadge.find_by(:player_id => @player_season.player.id, :season_id => Season.last.id)
	 		@player_badge.update(:prva_asistencija => 1)
	 	end 
	end

	def increment_player_season_assist
		#PlayerSeason.first.update(:assists => 7)
		PlayerSeason.increment_counter(:assists, self.player_season)
	end

	def increment_current_match_assist
		#PlayerSeason.first.update(:assists => 7)
		if self.is_home
			Match.increment_counter(:home_assists, self.match)
		else
			Match.increment_counter(:away_assists, self.match)
		end
	end

	######################################################################
	# assists_in_chain == 2 jer racuna od nule, a after_create je u pitanju
	# Ova metoda kreira assits in chain i kad dodje na 3 asistencije resetuje 
	# se na nulu i doda plus 1 na bedz koji se zove tri_asist_na_utakmici 
	######################################################################

	def increment_assists_in_chain
		@player_season = PlayerSeason.find_by(:id => self.player_season)
		@broj_asistencija_u_nizu = @player_season.assists_in_chain + 1
		@player_season.update(:assists_in_chain => @broj_asistencija_u_nizu)
		#if self.player_season[:assists_in_chain] == 2
			#@player_badge = PlayerBadge.find_by(:id => self.player_season.player.player_badge.id)
			#@tri_asistencije = @player_badge.tri_asist_na_utakmici + 1
			#@player_badge.update(:tri_asist_na_utakmici => @tri_asistencije)
			#self.player_season.update(:assists_in_chain => 0)
		#end
	end

	def increment_team_season_assists
		@assist = TeamSeason.find_by(:team_id => self.player_season.player.team.id, :season_id => Season.last.id)
		TeamSeason.increment_counter(:assists, @assist)
	end
end
  