class Contactmessage
  include ActiveModel::Model
  attr_accessor :name, :email, :subject, :content

  def mail_subject
  	"#{subject} #{name} #{email}"
  end

  validates :name, :email, :subject, :content, presence: true
end