class Team < ActiveRecord::Base
  after_create :create_new_team_season,
               :create_new_team_badges,
               :create_new_team_players,
               :create_home_team,
               :create_away_team

  before_destroy :destroy_away_team,
                 :destroy_home_team,
                 :destroy_new_team_players,
                 :destroy_new_team_badges,
                 :destroy_new_team_season

  has_many   :players, :dependent => :destroy
  has_one    :home_team
  has_one    :away_team
  has_many   :team_season
  has_many   :team_badge
  has_one    :team_player
  belongs_to :league
  
  accepts_nested_attributes_for :players
  # Dva reda ispod dolaze uz Paperclip gem
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>", :logo => "40x40>"}, :default_url => lambda { |avatar| avatar.instance.set_default_url}
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates :name, presence:true

  def set_default_url
    ActionController::Base.helpers.asset_path('default_team.png')
  end

  # Attributes for RAILS REST API
  def avatar_url
    "http://balf.rs#{self.avatar.url(:original)}"
  end

  def team_season_id
    self.team_season.id
  end

  def team_badge_id
    self.team_badge.id
  end
  private
  
  ########################################################
  # AFTER CREATE TEAM
  ########################################################
	def create_new_team_season
		@team=Team.where(:id => self.id)
		@season = Season.last
		TeamSeason.create(:season_id => @season.id, :team_id => self.id)
	end

  # Metoda koja kreira sve bedzeve za jedan tim
  def create_new_team_badges
    #@team=Team.where(:id= => self.id)
    TeamBadge.create(:team_id => self.id, :season_id => Season.last.id)
  end

  # Ova tabela sluzi za postavku grupne slike tima
  def create_new_team_players
    #@team=Team.where(:id= => self.id)
    TeamPlayer.create(:team_id => self.id)
  end

  def create_home_team
    HomeTeam.create(:team_id => self.id)
  end

  def create_away_team
    AwayTeam.create(:team_id => self.id)
  end

  ########################################################
  # BEFORE DESTROY TEAM
  ########################################################
  def destroy_new_team_season
    @team=Team.where(:id => self.id)
    @season = Season.last
    TeamSeason.where(:season_id => @season.id, :team_id => self.id).destroy_all
  end

  # Metoda koja kreira sve bedzeve za jedan tim
  def destroy_new_team_badges
    #@team=Team.where(:id= => self.id)
    TeamBadge.where(:team_id => self.id).destroy_all
  end

  # Ova tabela sluzi za postavku grupne slike tima
  def destroy_new_team_players
    #@team=Team.where(:id= => self.id)
    TeamPlayer.where(:team_id => self.id).destroy_all
  end

  def destroy_home_team
    HomeTeam.where(:team_id => self.id).destroy_all
  end

  def destroy_away_team
    AwayTeam.where(:team_id => self.id).destroy_all
  end
end