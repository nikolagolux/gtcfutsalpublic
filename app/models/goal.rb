class Goal < ActiveRecord::Base
	after_create :prvi_gol,
				 :increment_player_season_goal,
				 :increment_current_match_goal,
				 :increment_goals_in_chain

	belongs_to :match
	belongs_to :player_season 

	private
	def prvi_gol
		@player_season = PlayerSeason.find_by(:id => self.player_season)
		# Logika za dodelu bedza prvi gol
		if @player_season[:goals] == 0
			@player_badge = PlayerBadge.find_by(:player_id => @player_season.player.id, :season_id => Season.last.id)
			@player_badge.update(:prvi_gol => 1)
		end
	end

	def increment_player_season_goal
		#PlayerSeason.first.update(:goals => 7)
		PlayerSeason.increment_counter(:goals, self.player_season)
	end

	def increment_current_match_goal
		#PlayerSeason.first.update(:goals => 7)
		if self.is_home
			Match.increment_counter(:home_goals, self.match)
			# increment_team_season_goal_scored
			@goal_scored = TeamSeason.find_by(:team_id => self.player_season.player.team.id, :season_id => Season.last.id)
			TeamSeason.increment_counter(:goals_scored, @goal_scored)
			# increment_team_season_goal_received
			@goal_received = TeamSeason.find_by(:team_id => self.match.away_team.team.id, :season_id => Season.last.id)
			TeamSeason.increment_counter(:goals_received, @goal_received)
		else 
			Match.increment_counter(:away_goals, self.match)
			# increment_team_season_goal_scored
			@goal_scored = TeamSeason.find_by(:team_id => self.player_season.player.team.id, :season_id => Season.last.id)
			TeamSeason.increment_counter(:goals_scored, @goal_scored)
			# increment_team_season_goal_received
			@goal_received = TeamSeason.find_by(:team_id => self.match.home_team.team.id, :season_id => Season.last.id)
			TeamSeason.increment_counter(:goals_received, @goal_received)
		end
	end


	######################################################################
	# goals_in_chains == 2 jer racuna od nule, a after_create je u pitanju
	# Ova metoda kreira goals in chain i kad dodje na 3 gola resetuje se na
	# nulu i doda plus 1 na bedz koji se zove tri_gola_na_utakmici
	######################################################################
	
	def increment_goals_in_chain
		@player_season = PlayerSeason.find_by(:id => self.player_season)

		# Logika za hat trick
		@broj_golova_u_nizu = @player_season.goals_in_chain + 1		
		@player_season.update(:goals_in_chain => @broj_golova_u_nizu)
		#if self.player_season[:goals_in_chain] == 2
			#@player_badge = PlayerBadge.find_by(:id => self.player_season.player.player_badge.id)
			#@tri_gola = @player_badge.tri_gola_na_utakmici + 1
			#@player_badge.update(:tri_gola_na_utakmici => @tri_gola)
			#self.player_season.update(:goals_in_chain => 0)
		#end 
	end

end
 