class GoalkeeperSave < ActiveRecord::Base
	after_create :increment_player_season_goalkeeper_save,
				 :increment_current_match_goalkeeper_save,
				 :increment_counter_gk_saves_in_chain

	belongs_to :match
	belongs_to :player_season

	private
	def increment_player_season_goalkeeper_save
		#PlayerSeason.first.update(:fouls => 7)
		PlayerSeason.increment_counter(:goalkeeper_saves, self.player_season)
	end

	def increment_current_match_goalkeeper_save
		#PlayerSeason.first.update(:fouls => 7)
		if self.is_home
			Match.increment_counter(:home_goalkeeper_saves, self.match)
		else
			Match.increment_counter(:away_goalkeeper_saves, self.match)
		end
	end

	def increment_counter_gk_saves_in_chain
		@player_season = PlayerSeason.find_by(:id => self.player_season)
		@broj_odbrana_u_nizu = @player_season.gk_saves_in_chain + 1
		@player_season.update(:gk_saves_in_chain => @broj_odbrana_u_nizu)
	end
end
