class PlayerBadge < ActiveRecord::Base
	#############################################################
	# Ne smemo da imamo vise player_bedzeva sa istim player_id
	# i istim season_id-em
	#############################################################
	validates_uniqueness_of :player_id, :scope => :season_id
	#############################################################

	belongs_to :player
end
