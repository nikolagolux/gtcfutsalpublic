class TeamBadge < ActiveRecord::Base
	#############################################################
	# Ne smemo da imamo vise team_bedzeva sa istim team_id
	# i istim season_id-em
	#############################################################
	validates_uniqueness_of :team_id, :scope => :season_id
	#############################################################
	belongs_to :team
end
