class YellowCard < ActiveRecord::Base
	after_create :increment_player_season_yellow_card,
				 :increment_current_match_yellow_card,
				 :increment_team_season_total_yellow_card

	belongs_to :match
	belongs_to :player_season

	private
	def increment_player_season_yellow_card
		#PlayerSeason.first.update(:yellow_cards => 7)
		PlayerSeason.increment_counter(:yellow_cards, self.player_season)
	end

	def increment_current_match_yellow_card
		#PlayerSeason.first.update(:yellow_cards => 7)
		if self.is_home
			Match.increment_counter(:home_yellow_cards, self.match)
		else
			Match.increment_counter(:away_yellow_cards, self.match)
		end
	end

	def increment_team_season_total_yellow_card
		@total_yellow_cards = TeamSeason.find_by(:team_id => self.player_season.player.team.id, :season_id => Season.last.id)
		TeamSeason.increment_counter(:total_yellow_cards, @total_yellow_cards)
	end
end