class ContactmessageMailer < ApplicationMailer
  # use your own email address here
  default :to => "balf.kontakt@gmail.com"

  def message_me(msg)
    @msg = msg
    mail from: @msg.email, subject: @msg.mail_subject, body: @msg.content   
  end
end
