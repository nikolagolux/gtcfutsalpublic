json.array(@svi_igraci) do |player|
  json.extract! player, :id, :name, :surname, :avatar_url, :level
  json.url player_url(player, format: :json)
end
