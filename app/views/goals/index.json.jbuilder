json.array!(@goals) do |goal|
  json.extract! goal, :id, :match_id, :player_season_id, :is_home
  json.url article_url(goal, format: :json)
end
