class FoulsController < ApplicationController
  before_action :is_match_started

  def index
    @fouls = Foul.all 
  end

  def new
    # @foul = Foul.new
    # @player_seasons = PlayerSeason.all
    # @player = Player.where(:id => :player_id)
  end

  def new_home
    @foul = Foul.new
    # Ovde u @player_season treba da oznacimo sve igrace iz HOME tima
    #@player = Player.where(:id => :player_id)
    # Ovde u @player_season treba da oznacimo sve igrace iz HOME tima
    @home_team = current_match.home_team.team
    @players = Player.where(:team_id => @home_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      @player_season = PlayerSeason.where(:player_id => player.id)
      if i == 0 then
        @player_seasons = @player_season
      else 
        @player_seasons.concat(@player_season)
      end
      i = 1
    end
    session[:is_home] = true
    # U promenljivu match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @match_players = MatchPlayer.where(
      :match_id => current_match.id,
      :is_home => session[:is_home])

    # Ovde trazimo player_sezone za izabrane igrace,
    # glavna promenljiva je player_seasons_on_match
    i = 0
    @match_players.each do |match_player|
      @player_season_on_match = PlayerSeason.where(:id => match_player.player_season_id)
      if i == 0 then
        @player_seasons_on_match = @player_season_on_match
      else
        @player_seasons_on_match.concat(@player_season_on_match)
      end
      i = 1
    end
  end

  def new_away
    @foul = Foul.new
    # Ovde u @player_season treba da oznacimo sve igrace iz AWAY tima
    # @team = Team.find() ...
    # @player = Player.where(:team_id => self.team)
    @away_team = current_match.away_team.team
    @players = Player.where(:team_id => @away_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      @player_season = PlayerSeason.where(:player_id => player.id)
      if i == 0 then
        @player_seasons = @player_season
      else 
        @player_seasons.concat(@player_season)
      end
      i = 1
    end
    session[:is_home] = false
    # U promenljivu match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @match_players = MatchPlayer.where(
      :match_id => current_match.id,
      :is_home => session[:is_home])

    # Ovde trazimo player_sezone za izabrane igrace,
    # glavna promenljiva je player_seasons_on_match
    i = 0
    @match_players.each do |match_player|
      @player_season_on_match = PlayerSeason.where(:id => match_player.player_season_id)
      if i == 0 then
        @player_seasons_on_match = @player_season_on_match
      else
        @player_seasons_on_match.concat(@player_season_on_match)
      end
      i = 1
    end 
  end

  def create
    #************************************************************
    # Faulovi ne mogu da se kreiraju ako match nije startovan
    #************************************************************
    if current_match.is_match_started
     @foul = Foul.new(foul_params)
     # Dodeljujemo datom faul da je dat na odredjenoj utakmici
     @foul.match_id = current_match.id
     # Pitamo se da li je faul dao domacin ili gost
     @pauza = (current_match.second_half_started - current_match.first_half_ended)
     @foul_scored_m = (DateTime.now.seconds_since_midnight - @pauza - current_match.match_started).to_f / 60

     @foul.created_at = @foul_scored_m.round
     
     @foul.is_home = session[:is_home]
      if @foul.save
        flash[:success] = "Foul added!"
        redirect_to match_path(current_match)
      else
        render 'new'
      end
    end
  end

  def edit
  end

  def update
  end

  def show
  end

  private
  def foul_params
    params.require(:foul).permit( :match_id, :player_season_id, :is_home)
  end

end
