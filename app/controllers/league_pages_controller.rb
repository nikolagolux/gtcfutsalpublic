class LeaguePagesController < ApplicationController
  before_action :set_league, only:[:edit, :update, :show]

  def index
    @leagues = League.all
  end

  def fourplusone
    @four_plus_one_leagues = League.where(:is_fourplusone => "1")
  end

  def fiveplusone
    @five_plus_one_leagues = League.where(:is_fiveplusone => "1")
  end

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  # Tabela (Standardna tabela po ligama, bez EXPIRIENECE)
  def show
    @teams = Team.where(:league_id => @league.id)

    if @teams
        # Ovde trazimo team_sezone
        i = 0
        @teams.each do |team|
          @team_season = TeamSeason.where(:team_id => team.id)
          if i == 0 then
            @team_seasons = @team_season
          else 
            @team_seasons.concat(@team_season)
          end
          i = 1
        end
    end

    if @team_seasons
        # Logika za sortiranje timova
        pozicija = 1
        @team_seasons.sort_by{|e| -e[:points]}.each do |team_season|
          team_season.update(:position => pozicija)
          pozicija += 1
        end

        # Trazimo igrace iz date lige
        i = 0
        @teams.each do |team|
          @players_per_one_team = Player.where(:team_id => team.id)
          if i == 0 then
            @players = @players_per_one_team
          else 
            @players.concat(@players_per_one_team)
          end
          i = 1
        end
        # sada se u @players nalaze svi igraci iz odredjene lige
    end

    if @players
        i = 0
        @players.each do |player|
          @player_season = PlayerSeason.where(:player_id => player.id)
          if i == 0 then
            @player_seasons = @player_season
          else 
            @player_seasons.concat(@player_season)
          end
          i = 1
        end
        # sada imamo sve @player_seasons
    end

    if @player_seasons
        # Logika za sortiranje igraca
        pozicija = 1
        @player_seasons.sort_by{|e| -e[:goals]}.each do |player_season|
          player_season.update(:position => pozicija)
          pozicija += 1
        end
    end

    @articles = Article.where(:league_id => @league.id).paginate(page: params[:page], per_page: 6 ).order('created_at DESC')

  end

  private
  def set_league
    @league = League.find_by(:id => params[:id])
    commontator_thread_show(@league)
  end

  def league_params
    params.require(:league).permit(:name)
  end
end
