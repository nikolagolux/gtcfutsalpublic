class ContactmessagesController < ApplicationController
  def new
    @message = Contactmessage.new
  end

  def create
    @message = Contactmessage.new(message_params)

    if @message.valid?
      ContactmessageMailer.message_me(@message).deliver_now
      redirect_to kontakt_path, notice: "Thank you for your message."
    else
      #render :new
      redirect_to "welcome_pages/contact"
    end
  end

  private
    def message_params
      params.require(:contactmessage).permit(:name, :email, :subject, :content)
    end 
end
