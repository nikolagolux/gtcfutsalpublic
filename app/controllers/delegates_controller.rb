class DelegatesController < ApplicationController
  #validates :if_delegate
  before_action :set_delegate, only:[:edit, :update, :show]

  def index
  	@delegates = Delegate.all
  end

  def new
    @delegate = Delegate.new
    @user = @delegate.build_user
  end

  def create
   @delegate = Delegate.new(delegate_params)
    if @delegate.delegate_registration_token == Season.last.delegate_registration_token
        if @delegate.save
          #flash[:success] = "Welcome #{@player.name}"
          @delegate_registration_token = rand(1051..9958)
          Season.last.update(:delegate_registration_token => @delegate_registration_token)
          flash[:success] = "Dobrodošli u BALF ligu, uspešno ste se registrovali!"
          redirect_to root_path
        else
          render 'new'
        end
    else
        flash[:danger] = "Uneli ste neispravan registracioni token!"
        render 'new'
    end
  end

  def edit
  end

  def update
    if @delegate.update!(delegate_profile_params)
      flash[:success] = "Your account was updated successfully"
      redirect_to delegate_path
    end
  end

  def show
  	# Pogledati OBAVEZNO!!! Vazno da se nauci WHERE
    @matches = Match.where(:delegate_id => current_user.delegate.id, :is_match_finished => 0, :season_id => Season.last.id)
    @matches_finished = Match.where(:delegate_id => current_user.delegate_id, :is_match_finished => 1, :season_id => Season.last.id)
  end

  private
  def set_delegate
    @delegate = current_user.delegate
  end

  def delegate_params
    params.require(:delegate).permit(:name, :surname, :delegate_registration_token, user_attributes: [:email, :password])
  end

  def delegate_profile_params
    params.require(:delegate).permit(:name, :surname, user_attributes: [:username, :email, :password])
  end
end
