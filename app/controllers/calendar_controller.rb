class CalendarController < ApplicationController

  def index
  end

  def show
  	@matches_where_team_is_home = Match.where(:home_team_id => current_user.player.team.home_team.id)
  	@matches_where_team_is_away = Match.where(:away_team_id => current_user.player.team.away_team.id)
  	@matches = @matches_where_team_is_home
  	@matches.concat(@matches_where_team_is_away)
  end

  def update
  end
end
