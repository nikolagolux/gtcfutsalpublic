class PlayersController < ApplicationController
  # before_action :if_player, only:[:show]
  before_action :set_player, only:[:edit, :update, :show]
  before_action :require_same_player, only:[:edit, :update]

  def index
  	@players = Player.paginate(page: params[:page], per_page: 10)
    @svi_igraci = Player.all
    #render :json => @igraci
    #render :json => @igraci.to_json(:methods => [:avatar_url])
    #respond_to do |format|
      #format.html { render :nothing => true }
      #format.js   { render :partial => 'profiles/update.js' }
      #format.json { render :json => @igraci.to_json(:methods => [:avatar_url])}
    #end
  end

  def new
    @player = Player.new
    #@team = @player.build_team
    @user = @player.build_user
  end

  def create
   @player = Player.new(player_params)
    if @player.player_registration_token == Season.last.player_registration_token
        if @player.save
          #flash[:success] = "Welcome #{@player.name}"
          @player_registration_token = rand(1051..9958)
          Season.last.update(:player_registration_token => @player_registration_token)
          flash[:success] = "Dobrodošli u BALF ligu, uspešno ste se registrovali!"
          redirect_to root_path
        else
          render 'new'
        end
    else
        flash[:danger] = "Uneli ste neispravan registracioni token!"
        render 'new'
    end
  end

  def edit 
  end

  def update
    if @player.update!(player_profile_params)
      flash[:success] = "Your account was updated successfully"
      redirect_to player_path(current_user.player)
    else
      render 'edit'
    end
  end

  def show
    @player = Player.find(params[:id])
    @player_season = PlayerSeason.find_by(:season_id => Season.last.id, :player_id => @player.id)
    ##########################################
    # Metode za tabelu ispod slike na profilu 
    # igraca
    ##########################################
    @korisnicko_ime = @player.user.username
    @ime = @player.name
    @prezime = @player.surname
    @mail = @player.user.email
    @datum_rodjenja = @player.birthdate

    @broj_pobeda = @player_season.matches_win
    @broj_neresenih = @player_season.matches_draw
    @broj_izgubljenih = @player_season.matches_lose

    @broj_odigranih_meceva = @broj_pobeda + @broj_neresenih + @broj_izgubljenih
    @player_season.update(:matches_played => @broj_odigranih_meceva)

    
    @procenat_pobeda = @broj_pobeda.to_f * 100 / @broj_odigranih_meceva
    @procenat_neresenih = @broj_neresenih.to_f * 100 / @broj_odigranih_meceva
    @procenat_izgubljenih = @broj_izgubljenih.to_f * 100 / @broj_odigranih_meceva

    @level = @player_season[:level]
    # Izracunavanje koliko procenata od iduceg levela je osvojio igrac
    if @level == 1
      @procenata_za_iduci_level = (100*@player_season[:expirience])/250
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 2
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-250)/1250)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 3
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-1500)/2000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 4
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-3500)/2500)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 5
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-6000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 6
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-10000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 7
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-14000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 8
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-18000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 9
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-22000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 10
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-26000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 11
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-30000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 12
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-34000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 13
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-38000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 14
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-42000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 15
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-46000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 16
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-50000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 17
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-54000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 18
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-58000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 19
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-62000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 20
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-66000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 21
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-70000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 22
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-74000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 23
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-78000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 24
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-82000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 25
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-86000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 26
      @procenata_za_iduci_level = (100*(@player_season[:expirience]-90000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    end
  end

  private
  def set_player
    @player = Player.find(params[:id])
  end

  def require_same_player
    if current_user.player != @player
      flash[:danger] = "Možete da menjate samo svoje informacije!"
      redirect_to root_path
    end
  end

  def player_params
    params.require(:player).permit(:name, :surname, :JMBG, :avatar, :player_registration_token, user_attributes: [:email, :password])
  end

  def player_profile_params
    params.require(:player).permit(:name, :surname, :JMBG, :avatar, user_attributes: [:username, :email, :password])
  end
end
