class Api::V1::MatchPlayersController < ApplicationController
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token

  def index
    @match_players = MatchPlayer.all

    i = 0
    @match_players.each do |match_player|
      if match_player.player_season_id != nil
          @match_players[i] = match_player
      end
    end
    respond_to do |format|
      format.json { render json: @match_players }
      format.xml { render xml: @match_players }
    end
  end

  def new
  end

  def new_home
  end

  def new_away
  end

  def create
  end

  def chooseplayers
    # ovde su svi id-evi igraca koji su izabrani u checkbox-ovima
    # da su dosli na utakmicu
    @match_player = MatchPlayer.new(match_player_params)
    # Dodeljujemo datom golu da je dat na odredjenoj utakmici
    @current_match = Match.find_by(:id => @match_player.match_id)

      #@match_player.save(
      #  :player_season_id => @match_player.player_season_id,
      #  :match_id => @current_match.id,
      #  :is_home => @match_player.is_home
      #)
    respond_to do |format|
      if @match_player.save(
        :player_season_id => @match_player.player_season_id,
        :match_id => @current_match.id,
        :is_home => @match_player.is_home
        )
        format.json { render json: @match_player, status: :created }
        format.xml { render xml: @match_player, status: :created }
      else
        format.json { render json: @match_player.errors, status: :unprocessable_entity }
        format.xml { render xml: @match_player.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
  end

  def show
  end

  private

  def match_player_params
    params.require(:match_player).permit(:match_id, :player_season_id, :is_home)
  end

end




