class Api::V1::PlayerSeasonsController < ApplicationController
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token
  before_filter :fetch_player_season, :except => [:index, :create]

  def fetch_player_season
    @player_season = PlayerSeason.find_by_id(params[:id])
  end

  def index
    @player_seasons = PlayerSeason.all
    respond_to do |format|
      format.json { render json: @player_seasons }
      format.xml { render xml: @player_seasons }
    end
  end

  def new
  end

  def edit
  end

  def show
    respond_to do |format|
      format.json { render json: @player_season }
      format.xml { render xml: @player_season }
    end
  end
end