class Api::V1::AwayTeamsController < ApplicationController
  def index
  	@away_teams = AwayTeam.all
  	respond_to do |format|
      format.json { render json: @away_teams }
      format.xml { render xml: @away_teams }
    end
  end

  def new
  end

  def edit
  end

  def show
    respond_to do |format|
      format.json { render json: @away_team }
      format.xml { render xml: @away_team }
    end
  end

  private
  def set_away_team
    @away_team = AwayTeam.find(params[:id])
  end
end