class Api::V1::ArticlesController < ApplicationController
  #http_basic_authenticate_with :name => "myfinance", :password => "credit123"
  
  #http_basic_authenticate_with :name => "admin", :password => "secret"
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token

  def index
    @articles = Article.all
    respond_to do |format|
      format.json { render json: @articles }
      format.xml { render xml: @articles }
    end
  end

  def show
    respond_to do |format|
      format.json { render json: @article }
      format.xml { render xml: @article }
    end
  end

  def create
    @article = Article.new(article_params)
    respond_to do |format|
      if @article.save
        format.json { render json: @article, status: :created }
        format.xml { render xml: @article, status: :created }
      else
        format.json { render json: @article.errors, status: :unprocessable_entity }
        format.xml { render xml: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @article.update_attributes(params[:article])
        format.json { head :no_content, status: :ok }
        format.xml { head :no_content, status: :ok }
      else
        format.json { render json: @article.errors, status: :unprocessable_entity }
        format.xml { render xml: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @article.destroy
        format.json { head :no_content, status: :ok }
        format.xml { head :no_content, status: :ok }
      else
        format.json { render json: @article.errors, status: :unprocessable_entity }
        format.xml { render xml: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :description, :user_id)
    end
end