class Api::V1::TeamBadgesController < ApplicationController
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token
  before_filter :fetch_team_badge, :except => [:index]

  def fetch_team_badge
    @team_badge = TeamBadge.find_by_id(params[:id])
  end

  def index
    @team_badges = TeamBadge.all
    respond_to do |format|
      format.json { render json: @team_badges }
      format.xml { render xml: @team_badges }
    end
  end

  def new
  end

  def edit
  end

  def show
    respond_to do |format|
      format.json { render json: @team_badge }
      format.xml { render xml: @team_badge }
    end
  end
end
 