class Api::V1::PlayersController < ApplicationController
  #http_basic_authenticate_with :name => "myfinance", :password => "credit123"
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  before_filter :fetch_player, :except => [:index, :create]

  def fetch_player
    @player = Player.find_by_id(params[:id])
  end

  def index
    @players = Player.all
    respond_to do |format|
      format.json { render json: @players }
      format.xml { render xml: @players }
    end
  end

  def show
    respond_to do |format|
      format.json { render json: @player }
      format.xml { render xml: @player }
    end
  end

  def create
    @player = Player.new(params[:player])
    @player.temp_password = Devise.friendly_token
    respond_to do |format|
      if @player.save
        format.json { render json: @player, status: :created }
        format.xml { render xml: @player, status: :created }
      else
        format.json { render json: @player.errors, status: :unprocessable_entity }
        format.xml { render xml: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @player.update_attributes(params[:player])
        format.json { head :no_content, status: :ok }
        format.xml { head :no_content, status: :ok }
      else
        format.json { render json: @player.errors, status: :unprocessable_entity }
        format.xml { render xml: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @player.destroy
        format.json { head :no_content, status: :ok }
        format.xml { head :no_content, status: :ok }
      else
        format.json { render json: @player.errors, status: :unprocessable_entity }
        format.xml { render xml: @player.errors, status: :unprocessable_entity }
      end
    end
  end
end