class Api::V1::PlayerBadgesController < ApplicationController
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token
  before_filter :fetch_player_badge, :except => [:index, :create]

  def fetch_player_badge
    @player_badge = PlayerBadge.find_by_id(params[:id])
  end

  def index
    @player_badges = PlayerBadge.all
    respond_to do |format|
      format.json { render json: @player_badges }
      format.xml { render xml: @player_badges }
    end
  end

  def new
  end

  def edit
  end

  def show
    respond_to do |format|
      format.json { render json: @player_badge }
      format.xml { render xml: @player_badge }
    end
  end
end