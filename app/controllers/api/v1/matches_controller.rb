class Api::V1::MatchesController < ApplicationController
  before_action :set_match, only:[:edit, 
                                  :update, 
                                 :show, 
                                  :change_match_started,
                                  :change_first_half_ended,
                                  :change_second_half_started,
                                  :change_second_half_ended]
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token

  def index
    @matches = Match.all
    respond_to do |format|
      format.json { render json: @matches }
      format.xml { render xml: @matches }
    end
  end

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def show
    respond_to do |format|
      format.json { render json: @match }
      format.xml { render xml: @match }
    end

    if logged_in?
      if current_user.delegate #&& !@match.is_match_finished
        # Ovde sesija treba automatski da se napravi
        # kada delegat poseti stranica
        if session[:match_id] != @match.id
          session[:match_id] = @match.id
        end
      end
    end

    if @match != nil
      @pauza = (@match.second_half_started - @match.first_half_ended)
      @minut_utakmice = (DateTime.now.seconds_since_midnight - @pauza - @match.match_started).to_f / 60
      @minut_utakmice = @minut_utakmice.round + 1
    end

    ######################################
    # Definisan home team
    ######################################
    @home_team = @match.home_team.team
    @home_team_season = @home_team.team_season

    ######################################
    # Definisan away team
    ######################################
    @away_team = @match.away_team.team
    @away_team_season = @away_team.team_season

    ######################################
    # HOME TEAM PLAYER SEASONS ON MATCH
    ######################################
    # U promenljivu home_match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @home_match_players = MatchPlayer.where(
      :match_id => @match.id,
      :is_home => 1)

    # Ovde trazimo home_player_sezone za izabrane igrace,
    # glavna promenljiva je home_player_seasons_on_match
    i = 0
    @home_match_players.each do |home_match_player|
      @home_player_season_on_match = PlayerSeason.where(:id => home_match_player.player_season_id)
      if i == 0 then
        @home_player_seasons_on_match = @home_player_season_on_match
      else
        # HOME PLAYER_SEASONS ON MATCH
        @home_player_seasons_on_match.concat(@home_player_season_on_match)
      end
      i = 1
    end

    ######################################
    # AWAY TEAM PLAYER SEASONS ON MATCH
    ######################################
    # U promenljivu away_match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @away_match_players = MatchPlayer.where(
      :match_id => @match.id,
      :is_home => 0)

    # Ovde trazimo home_player_sezone za izabrane igrace,
    # glavna promenljiva je home_player_seasons_on_match
    i = 0
    @away_match_players.each do |away_match_player|
      @away_player_season_on_match = PlayerSeason.where(:id => away_match_player.player_season_id)
      if i == 0 then
        @away_player_seasons_on_match = @away_player_season_on_match
      else
        # AWAY PLAYER_SEASONS ON MATCH
        @away_player_seasons_on_match.concat(@away_player_season_on_match)
      end
      i = 1
    end

    @home_goals_on_match = Goal.where(:is_home => 1, :match_id => @match.id)
    @away_goals_on_match = Goal.where(:is_home => 0, :match_id => @match.id)  
  end

  def change_match_started
    # Metod .seconds_since_midnight vraca integer koliko sekundi je proslo od ponoci
    # tako mozemo da izracunamo u kojoj sekundi je postignut gol itd.
    #@match = Match.update(params)
    @match.update(:match_started => DateTime.now.seconds_since_midnight, :is_match_started => true)
    # Ranije se ovde sesija otvarala, sada u metodu show ovog kontrolera
    respond_to do |format|
      format.json { render json: @match }
      format.xml { render xml: @match }
    end
  end

  def change_first_half_ended
    respond_to do |format|
      format.json { render json: @match }
      format.xml { render xml: @match }
    end
    # Metod .seconds_since_midnight vraca integer koliko sekundi je proslo od ponoci
    # tako mozemo da izracunamo u kojoj sekundi je postignut gol itd.
    @match.update(:first_half_ended => DateTime.now.seconds_since_midnight)

    # OVDE LOGIKA O REZULTATU NA POLUVREMENU, UPDATE POBEDA,
    # IZGUBLJENIH, NERESENIH...
    # I ZA IGRACA I ZA TIM

  end

  def change_second_half_started
    respond_to do |format|
      format.json { render json: @match }
      format.xml { render xml: @match }
    end
    # Metod .seconds_since_midnight vraca integer koliko sekundi je proslo od ponoci
    # tako mozemo da izracunamo u kojoj sekundi je postignut gol itd.
    @match.update(:second_half_started => DateTime.now.seconds_since_midnight)
  end

  # LOGIKA kada se mec zavrsi
  def change_second_half_ended
    respond_to do |format|
      format.json { render json: @match }
      format.xml { render xml: @match }
    end
    # Metod .seconds_since_midnight vraca integer koliko sekundi je proslo od ponoci
    # tako mozemo da izracunamo u kojoj sekundi je postignut gol itd.
    @match.update(:match_finished => DateTime.now.seconds_since_midnight)


    ########################
    # Definicije za EXP
    ########################
    ########################
    # Vaznost utakmice
    ########################
    # 1) prijateljski mec, V = 1
    if @match.is_friendly
      @v = 1
    # 2) drugoligaski mec, V = 2.5
    elsif @match.is_second_league
      @v = 2.5
    # 3) kup, V = 3.0
    elsif @match.is_tournament
      @v = 3.0
    # 4) prvoligaski mec, V = 4.0
    elsif @match.is_first_league
      @v = 4.0
    end
 
    ###########################
    # Jacina protivnickog tima
    ###########################
    # Definisan home team
    @home_team = @match.home_team.team
    @home_team_season = @home_team.team_season
    # Definisan away team
    @away_team = @match.away_team.team
    @away_team_season = @away_team.team_season
    #############################
    # Racunamo jacinu HOME tima
    #############################
    # Za prvoplasiranu i poslednjih 15 ekipa postoje drugi kriterijumi za bodovanje
    # LIGA 4+1
    if @home_team.league.is_fourplusone
      if @home_team.team_season.position == nil || @home_team.team_season.position == 0
        @home_team.team_season.position = 0
        @jacina_domaceg_tima = 15
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @home_team.team_season.position == 1
        @jacina_domaceg_tima = 64
      # Standardna formula za racunanje bodova
      elsif @home_team.team_season.position > 1 && @home_team.team_season.position < 50
        @jacina_domaceg_tima = 64 - @home_team.team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @home_team.team_season.position > 49
        @jacina_domaceg_tima = 15
      end
    # LIGA 5+1
    elsif @home_team.league.is_fiveplusone
      if @home_team.team_season.position == nil || @home_team.team_season.position == 0
        @home_team.team_season.position = 0
        @jacina_domaceg_tima = 15
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @home_team.team_season.position == 1
        @jacina_domaceg_tima = 80
      # Standardna formula za racunanje bodova
      elsif @home_team.team_season.position > 1 && @home_team.team_season.position < 66
        @jacina_domaceg_tima = 80 - @home_team.team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @home_team.team_season.position > 65
        @jacina_domaceg_tima = 15
      end
    end
    @j_home = @jacina_domaceg_tima

    #############################
    # Racunamo jacinu AWAY tima
    #############################
        # Za prvoplasiranu i poslednjih 15 ekipa postoje drugi kriterijumi za bodovanje
    # LIGA 4+1
    if @away_team.league.is_fourplusone
      if @away_team.team_season.position == nil || @away_team.team_season.position == 0
        @away_team.team_season.position = 0
        @jacina_gostujuceg_tima = 15
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @away_team.team_season.position == 1
        @jacina_gostujuceg_tima = 64
      # Standardna formula za racunanje bodova
      elsif @away_team.team_season.position > 1 && @away_team.team_season.position < 50
        @jacina_gostujuceg_tima = 64 - @away_team.team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @away_team.team_season.position > 49
        @jacina_gostujuceg_tima = 15
      end
    # LIGA 5+1
    elsif @away_team.league.is_fiveplusone
      if @away_team.team_season.position == nil  || @away_team.team_season.position == 0
        @away_team.team_season.position = 0
        @jacina_gostujuceg_tima = 15
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @away_team.team_season.position == 1
        @jacina_gostujuceg_tima = 80
      # Standardna formula za racunanje bodova
      elsif @away_team.team_season.position > 1 && @away_team.team_season.position < 66
        @jacina_gostujuceg_tima = 80 - @away_team.team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @away_team.team_season.position > 65
        @jacina_gostujuceg_tima = 15
      end
    end
    @j_away = @jacina_gostujuceg_tima


    ###########################################################
    ###########################################################

    ########################
    # HOME TEAM
    ########################
    
    # U promenljivu home_match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @home_match_players = MatchPlayer.where(
      :match_id => @match.id,
      :is_home => 1)

    # Ovde trazimo home_player_sezone za izabrane igrace,
    # glavna promenljiva je home_player_seasons_on_match
    i = 0
    @home_match_players.each do |home_match_player|
      @home_player_season_on_match = PlayerSeason.where(:id => home_match_player.player_season_id)
      if i == 0 then
        @home_player_seasons_on_match = @home_player_season_on_match
      else
        # HOME PLAYER_SEASONS ON MATCH
        @home_player_seasons_on_match.concat(@home_player_season_on_match)
      end
      i = 1
    end

    ########################
    # AWAY TEAM
    ########################
   
    # U promenljivu away_match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @away_match_players = MatchPlayer.where(
      :match_id => @match.id,
      :is_home => 0)

    # Ovde trazimo home_player_sezone za izabrane igrace,
    # glavna promenljiva je home_player_seasons_on_match
    i = 0
    @away_match_players.each do |away_match_player|
      @away_player_season_on_match = PlayerSeason.where(:id => away_match_player.player_season_id)
      if i == 0 then
        @away_player_seasons_on_match = @away_player_season_on_match
      else
        # AWAY PLAYER_SEASONS ON MATCH
        @away_player_seasons_on_match.concat(@away_player_season_on_match)
      end
      i = 1
    end

    ########################
    # STATISTIKE MECA
    ########################
    @home_goals = @match.home_goals
    @away_goals = @match.away_goals
    @home_assists = @match.home_assists
    @away_assists = @match.away_assists
    @home_fouls = @match.home_fouls
    @away_fouls = @match.away_fouls
    @home_yellow_cards = @match.home_yellow_cards
    @away_yellow_cards = @match.away_yellow_cards
    @home_red_cards = @match.home_red_cards
    @away_red_cards = @match.away_red_cards
    @home_shoots = @match.home_shoots
    @away_shoots = @match.away_shoots

    # ... 

    ###################################
    # ANALIZA MECA, ko je pobedio itd.
    # SISTEM ZA BODOVANJE TIMA, IGRACA
    ###################################

    ################################################
    # Inkrement odigranih utakmica za igrace i ekipe
    ################################################

    # Za svakog home_playera_on_match dodajemo da je odigrao mec
      @home_player_seasons_on_match.each do |home_player_season|
        @domacin_odigrao_meceva = home_player_season.matches_played + 1
        home_player_season.update(:matches_played => @domacin_odigrao_meceva)
        #Logika za bedz odigrao je vise od 10 utakmica
        if (home_player_season[:matches_played] % 10 == 0) && (home_player_season[:matches_played] != 0)
          @odigrao_domacin_deset_utakmica = home_player_season.player.player_badge.odigrao_10_utakmica + 1
          home_player_season.player.player_badge.update(:odigrao_10_utakmica => @odigrao_domacin_deset_utakmica)
          #******************************
          # Exp za odigranih 10 utakmica
          #******************************
          @exp_za_10_odigranih_utakmica = home_player_season.expirience + 700
          home_player_season.update(:expirience => @exp_za_10_odigranih_utakmica)
        end
      end

    # Za svakog home_playera_on_match dodajemo da je odigrao mec
      @away_player_seasons_on_match.each do |away_player_season|
        @gost_odigrao_meceva = away_player_season.matches_played + 1
        away_player_season.update(:matches_played => @gost_odigrao_meceva)
        #Logika za bedz odigrao je vise od 10 utakmica
        if (away_player_season[:matches_played] % 10 == 0) && (away_player_season[:matches_played] != 0)
          @odigrao_gost_deset_utakmica = away_player_season.player.player_badge.odigrao_10_utakmica + 1
          away_player_season.player.player_badge.update(:odigrao_10_utakmica => @odigrao_gost_deset_utakmica)
          #******************************
          # Exp za odigranih 10 utakmica
          #******************************
          @exp_za_10_odigranih_utakmica_gost = away_player_season.expirience + 700
          away_player_season.update(:expirience => @exp_za_10_odigranih_utakmica_gost)
        end
      end

    # Za home_team_on_match dodajemo da je odigrao mec
      @domaci_tim_odigrao_utakmica = @home_team_season.matches_played + 1
      @home_team_season.update(:matches_played => @domaci_tim_odigrao_utakmica)
      

    # Za away_team_on_match dodajemo da je odigrao mec
      @gostujuci_tim_odigrao_utakmica = @away_team_season.matches_played + 1
      @away_team_season.update(:matches_played => @gostujuci_tim_odigrao_utakmica)



    ###################################
    # Logika za odredjivanje pobednika i dodavanje u bazu
    # DODELA EXP za POBEDU i NERESEN rezultat
    ###################################

    ##########################################################################################
    ## Pocetak prvog ifa (grananja) POBEDA DOMACINA
    ##########################################################################################
    #LOGIKA za pobedu domacina
    if @home_goals > @away_goals
      # DOMACIN POBEDIO
      @match.update(:is_home_win => 1)
      @match.update(:is_home_draw => 0)
      @match.update(:is_home_lose => 0)

      #Domaci tim je pobedio
      @pobeda_domaceg_tima = @home_team_season.matches_win + 1
      @home_team_season.update(:matches_win => @pobeda_domaceg_tima)
      
      #Poeni za pobedu domaceg tima
      # Poeni se ne dodeljuju ako je prijateljska ili kup
      if !@match.is_friendly && !@match.is_tournament
        @poeni_za_pobedu_tima = @home_team_season.points + 3
        @home_team_season.update(:points => @poeni_za_pobedu_tima)
      end

      #Bodovi za pobedu domaceg tima
      @bodovi_za_pobedu_domaceg_tima = @v*@j_away*3
      @bodovi_za_pobedu_domaceg_tima.round
      @bodovi_za_pobedu_domaceg_tima += @home_team_season.expirience
      @home_team_season.update(:expirience => @bodovi_za_pobedu_domaceg_tima)

      #Odredjivanje levela tima u odnose na osvojene bodove
      odredjivanje_levela(@home_team_season)
      @home_team_season.update(:level => @level)
      

      #Gostujuci tim je izgubio
      @poraz_gostujuceg_tima = @away_team_season.matches_lose + 1
      @away_team_season.update(:matches_lose => @poraz_gostujuceg_tima)
      # Za svakog home_playera_on_match dodajemo da je pobedio
      @home_player_seasons_on_match.each do |home_player_season|
        @pobedio_meceva = home_player_season.matches_win + 1
        home_player_season.update(:matches_win => @pobedio_meceva)

        #Bodovi za pobedu domacih igraca
        @bodovi_za_pobedu_domacih_igraca = @v*@j_away*3
        @bodovi_za_pobedu_domacih_igraca.round
        @bodovi_za_pobedu_domacih_igraca += home_player_season.expirience
        home_player_season.update(:expirience => @bodovi_za_pobedu_domacih_igraca)
        @level = home_player_season.level

        #Odredjivanje levela igraca u odnose na osvojene bodove
        odredjivanje_levela(home_player_season)
        home_player_season.update(:level => @level)
      end

      # Za svakoh away_player_on_match dodajemo da je izgubio
      @away_player_seasons_on_match.each do |away_player_season|
        @izgubio_meceva = away_player_season.matches_lose + 1
        away_player_season.update(:matches_lose => @izgubio_meceva)
      end
    ###################################################################################################
    ##   Kraj prvog ifa
    ###################################################################################################

    ###################################################################################################
    ###################################################################################################

    ###################################################################################################
    ## Pocetak drugog ifa NERESEN REZULTAT
    ###################################################################################################    
    # LOGIKA za neresen rezultat
    elsif @home_goals == @away_goals
      # Neresena utakmica
      @match.update(:is_home_win => 0)
      @match.update(:is_home_draw => 1)
      @match.update(:is_home_lose => 0)
      
      # Domaci tim je odigrao nereseno
      @nereseno_domaceg_tima = @home_team_season.matches_draw + 1
      @home_team_season.update(:matches_draw => @nereseno_domaceg_tima)

      # Poeni za neresnu domaceg tima
      # Poeni se ne dodeljuju ako je prijateljska ili kup
      if !@match.is_friendly && !@match.is_tournament
      @poeni_za_neresenu_domaceg_tima = @home_team_season.points + 1
      @home_team_season.update(:points => @poeni_za_neresenu_domaceg_tima)
      end

      # Bodovi za nereseno domaceg tima
      @bodovi_za_nereseno_domaceg_tima = @v*@j_away*1
      @bodovi_za_nereseno_domaceg_tima.round
      @bodovi_za_nereseno_domaceg_tima += @home_team_season.expirience
      @home_team_season.update(:expirience => @bodovi_za_nereseno_domaceg_tima)

      #Odredjivanje levela tima u odnose na osvojene bodove
      odredjivanje_levela(@home_team_season)
      @home_team_season.update(:level => @level)

      #Gostujuci tim je odigrao nereseno
      @nereseno_gostujceg_tima = @away_team_season.matches_draw + 1
      @away_team_season.update(:matches_draw => @nereseno_gostujceg_tima)

      #Poeni za neresnu gostujuceg tima
      # Poeni se ne dodeljuju ako je prijateljska ili kup
      if !@match.is_friendly && !@match.is_tournament
      @poeni_za_neresenu_gostujuceg_tima = @away_team_season.points + 1
      @away_team_season.update(:points => @poeni_za_neresenu_gostujuceg_tima)
      end

      #Bodovi za nereseno gostujuceg tima
      @bodovi_za_nereseno_gostujuceg_tima = @v*@j_home*1
      @bodovi_za_nereseno_gostujuceg_tima.round
      @bodovi_za_nereseno_gostujuceg_tima += @away_team_season.expirience
      @away_team_season.update(:expirience => @bodovi_za_nereseno_gostujuceg_tima)

      #Odredjivanje levela tima u odnose na osvojene bodove
      odredjivanje_levela(@away_team_season)
      @away_team_season.update(:level => @level)

      #Za svakog home_playera_on_match dodajemo da je odigrao nereseno
      @home_player_seasons_on_match.each do |home_player_season|
        @nereseno_meceva = home_player_season.matches_draw + 1
        home_player_season.update(:matches_draw => @nereseno_meceva)

        #************************************
        # Bodovi za nereseno domacih igraca
        #************************************
        @bodovi_za_nereseno_domacih_igraca = @v*@j_away*1
        @bodovi_za_nereseno_domacih_igraca.round
        @bodovi_za_nereseno_domacih_igraca += home_player_season.expirience
        home_player_season.update(:expirience => @bodovi_za_nereseno_domacih_igraca)
        @level = home_player_season.level

        #Odredjivanje levela igraca u odnose na osvojene bodove
        odredjivanje_levela(home_player_season)
        home_player_season.update(:level => @level)
      end

      #Za svakog away_player_on_match dodajemo da je odigrao nereseno
      @away_player_seasons_on_match.each do |away_player_season|
        @nereseno_meceva = away_player_season.matches_draw + 1
        away_player_season.update(:matches_draw => @nereseno_meceva)

        #*************************************
        # Bodovi za nereseno gostojucih igraca
        #*************************************
        @bodovi_za_nereseno_gostujucih_igraca = @v*@j_home*1
        @bodovi_za_nereseno_gostujucih_igraca.round
        @bodovi_za_nereseno_gostujucih_igraca += away_player_season.expirience
        away_player_season.update(:expirience => @bodovi_za_nereseno_gostujucih_igraca)
        @level = away_player_season.level

        #Odredjivanje levela igraca u odnose na osvojene bodove
        odredjivanje_levela(away_player_season)
        away_player_season.update(:level => @level)
      end

    ###################################################################################################
    ##   Kraj drugog ifa 
    ###################################################################################################

    ###################################################################################################
    ###################################################################################################

    ###################################################################################################
    ## Pocetak treceg ifa GOST POBEDIO
    ###################################################################################################
    elsif @away_goals > @home_goals
     #Logika za gosta da je pobedio
     #Gost je pobedio
      @match.update(:is_home_win => 0)
      @match.update(:is_home_draw => 0)
      @match.update(:is_home_lose => 1)

      #Domaci tim je izgubio
      @poraz_domaceg_tima = @home_team_season.matches_lose + 1
      @home_team_season.update(:matches_lose => @poraz_domaceg_tima)
      #Gostujuci tim je pobedio
      @pobeda_gostujuceg_tima = @away_team_season.matches_win + 1
      @away_team_season.update(:matches_win => @pobeda_gostujuceg_tima)

      #Poeni za pobedu gostujuceg tima
      # Poeni se ne dodeljuju ako je prijateljska ili kup
      if !@match.is_friendly && !@match.is_tournament
      @poeni_za_pobedu_gostujuceg_tima = @away_team_season.points + 3
      @away_team_season.update(:points => @poeni_za_pobedu_gostujuceg_tima)
      end

      #Bodovi za pobedu gostujuceg tima
      @bodovi_za_pobedu_gostujuceg_tima = @v*@j_home*3
      @bodovi_za_pobedu_gostujuceg_tima.round
      @bodovi_za_pobedu_gostujuceg_tima += @away_team_season.expirience
      @away_team_season.update(:expirience => @bodovi_za_pobedu_gostujuceg_tima)



      #Odredjivanje levela tima u odnose na osvojene bodove
      odredjivanje_levela(@away_team_season)
      @away_team_season.update(:level => @level)

      #Za svakog home_playera_on_match dodajemo da je izgubio
      @home_player_seasons_on_match.each do |home_player_season|
        @izgubljeno_meceva = home_player_season.matches_lose + 1
        home_player_season.update(:matches_lose => @izgubljeno_meceva)
      end

      #Za svakog away_player_on_match dodajemo da je pobedio
      @away_player_seasons_on_match.each do |away_player_season|
        @pobedjeno_meceva = away_player_season.matches_win + 1
        away_player_season.update(:matches_win => @pobedjeno_meceva)

        #Bodovi za pobedu igraca gostujuceg tima
      @bodovi_za_pobedu_gostujucih_igraca = @v*@j_home*3
      @bodovi_za_pobedu_gostujucih_igraca.round
      @bodovi_za_pobedu_gostujucih_igraca += away_player_season.expirience
      away_player_season.update(:expirience => @bodovi_za_pobedu_gostujucih_igraca)
      @level = away_player_season.level

        #Odredjivanje levela igraca u odnose na osvojene bodove
        odredjivanje_levela(away_player_season)
        away_player_season.update(:level => @level)
      end
    end
  ###################################################################################################
  ##   Kraj treceg ifa
  ###################################################################################################

  #######################
  ##   LOGIKA ZA BEDZEVE
  #######################

  ############################################################
  # LOGIKA ZA BEDZEVE TIMOVA
  ############################################################

  #***************************************************
  #*************************
  # Bedz za prvu pobedu tima
  #*************************
  #*************
  #Za domaci tim 
  #*************
  if @home_team_season[:matches_win] >= 1
    @home_team_season.team.team_badge.update(:prva_pobeda => true)
  else
    @home_team_season.team.team_badge.update(:prva_pobeda => false)
  end
  #********************************************
  # Logika za eksta EXP za prvu pobedu domacina
  #********************************************
  if @home_team_season[:matches_win] == 1
      @dodela_poena_za_prvu_pobedu_domacina = @home_team_season.expirience + 300
      @home_team_season.update(:expirience => @dodela_poena_za_prvu_pobedu_domacina)
  end
  #****************
  #Za gostujuci tim 
  #****************
  if @away_team_season[:matches_win] >= 1
    @away_team_season.team.team_badge.update(:prva_pobeda => true)
  else
    @away_team_season.team.team_badge.update(:prva_pobeda => false)
  end
  #*****************************************
  # Logika za eksta EXP za prvu pobedu gosta
  #*****************************************
  if @away_team_season[:matches_win] == 1
      @dodela_poena_za_prvu_pobedu_gosta = @away_team_season.expirience + 300
      @away_team_season.update(:expirience => @dodela_poena_za_prvu_pobedu_gosta)
  end
  #**************************************************
  #*****************************
  # Bedz za sedam i za tri uzastopne pobede
  #*****************************
  if @home_goals > @away_goals
    @pobede_domacina_za_redom = @home_team_season.wins_in_chain + 1
    @home_team_season.update(:wins_in_chain => @pobede_domacina_za_redom)
    @away_team_season.update(:wins_in_chain => 0)
    if @home_team_season[:wins_in_chain] % 3 == 0 && @home_team_season[:wins_in_chain] != 0
      @tri_pobede_domacina_u_nizu = @home_team_season.team.team_badge.tri_uzastopne_pobede + 1
      @home_team_season.team.team_badge.update(:tri_uzastopne_pobede => @tri_pobede_domacina_u_nizu)
      #******************************************
      # Logika za EXP za 3 pobed3 u nizu domacina 
      #******************************************
      @dodela_ekstra_poena_domacinu_za_tri_pobede_u_nizu = @home_team_season.expirience + 750
      @home_team_season.update(:expirience => @dodela_ekstra_poena_domacinu_za_tri_pobede_u_nizu)
    end
    if @home_team_season[:wins_in_chain] % 7 == 0 && @home_team_season[:wins_in_chain] != 0
      @sedam_pobeda_domacina_u_nizu = @home_team_season.team.team_badge.sedam_uzastopnih_pobeda + 1
      @home_team_season.team.team_badge.update(:sedam_uzastopnih_pobeda => @sedam_pobeda_domacina_u_nizu)
      #*******************************************
      # Logika za EXP za 7 pobeda u nizu domacina
      #*******************************************
      @dodela_ekstra_poena_domacinu_za_sedam_pobede_u_nizu = @home_team_season.expirience + 1700
      @home_team_season.update(:expirience => @dodela_ekstra_poena_domacinu_za_sedam_pobede_u_nizu)
    end
  elsif @home_goals == @away_goals
    @home_team_season.update(:wins_in_chain => 0)
    @away_team_season.update(:wins_in_chain => 0)
  elsif @away_goals > @home_goals
    @pobede_gostiju_za_redom = @away_team_season.wins_in_chain + 1
    @away_team_season.update(:wins_in_chain => @pobede_gostiju_za_redom)
    @home_team_season.update(:wins_in_chain => 0)
    if @away_team_season[:wins_in_chain] % 3 == 0 && @away_team_season[:wins_in_chain] != 0
      @tri_pobede_gosta_u_nizu = @away_team_season.team.team_badge.tri_uzastopne_pobede + 1
      @away_team_season.team.team_badge.update(:tri_uzastopne_pobede => @tri_pobede_gosta_u_nizu)
      #******************************************
      # Logika za EXP za 3 pobede u nizu gosta
      #******************************************
      @dodela_ekstra_poena_gostu_za_tri_pobede_u_nizu = @away_team_season.expirience + 750
      @away_team_season.update(:expirience => @dodela_ekstra_poena_gostu_za_tri_pobede_u_nizu)
    end
    if @away_team_season[:wins_in_chain] % 7 == 0 && @away_team_season[:wins_in_chain] != 0
      @sedam_pobeda_gostiju_u_nizu = @away_team_season.team.team_badge.sedam_uzastopnih_pobeda + 1
      @away_team_season.team.team_badge.update(:sedam_uzastopnih_pobeda => @sedam_pobeda_gostiju_u_nizu)
      #****************************************
      # Logika za EXP za 7 pobeda u nizu gosta
      #****************************************
      @dodela_ekstra_poena_gostu_za_sedamn_pobede_u_nizu = @away_team_season.expirience + 1700
      @away_team_season.update(:expirience => @dodela_ekstra_poena_gostu_za_sedamn_pobede_u_nizu)
    end
  end
  #**************************************************
  #*******************************************
  # Bedz za pobedu sa 10 i vise golova razlike
  #*******************************************
  if @home_goals-@away_goals >= 10
    @pobeda_domacina_sa_10_golova_razlike = @home_team_season.team.team_badge.pobeda_sa_10_golova_razlike + 1
    @home_team_season.team.team_badge.update(:pobeda_sa_10_golova_razlike => @pobeda_domacina_sa_10_golova_razlike)
    #******************************************************
    # Logika za EXP za pobedu domacina sa 10 i vise razlike
    #******************************************************
    # @exp_domacinu_10_plus_razlike oznacava dodela_expa_za_pobedu_domacina_sa_10_i_vise_razlike
    @exp_domacinu_10_plus_razlike = @home_team_season.expirience + 600
    @home_team_season.update(:expirience => @exp_domacinu_10_plus_razlike)
  end

  if @away_goals-@home_goals >= 10
    @pobeda_gosta_sa_10_golova_razlike = @away_team_season.team.team_badge.pobeda_sa_10_golova_razlike + 1
    @away_team_season.team.team_badge.update(:pobeda_sa_10_golova_razlike => @pobeda_gosta_sa_10_golova_razlike)
    #******************************************************
    # Logika za EXP za pobedu gosta sa 10 i vise razlike
    #******************************************************
    # @exp_domacinu_10_plus_razlike oznacava dodela_expa_za_pobedu_domacina_sa_10_i_vise_razlike
    @exp_gost_10_plus_razlike = @away_team_season.expirience + 600
    @away_team_season.update(:expirience => @exp_gost_10_plus_razlike)
  end
  #**************************************************
  #*******************************************
  # Bedz za dato vise od 10 golova na utakmici
  #*******************************************
  if @home_goals >= 10
    @domaci_tim_je_postigao_10_golova = @home_team_season.team.team_badge.dato_vise_od_10_golova + 1
    @home_team_season.team.team_badge.update(:dato_vise_od_10_golova => @domaci_tim_je_postigao_10_golova)
    #************************************************
    #Logika za exp za datih 10 i vise golova domacina
    #************************************************  
    @exp_domacin_10_plus_golova = @home_team_season.expirience + 500
    @home_team_season.update(:expirience => @exp_domacin_10_plus_golova)
  end

  if @away_goals >= 10
    @gostujuci_tim_je_postigao_10_golova = @away_team_season.team.team_badge.dato_vise_od_10_golova + 1
    @away_team_season.team.team_badge.update(:dato_vise_od_10_golova => @gostujuci_tim_je_postigao_10_golova)
    #*********************************************
    #Logika za exp za datih 10 i vise golova gosta
    #*********************************************  
    @exp_gost_10_plus_golova = @away_team_season.expirience + 500
    @away_team_season.update(:expirience => @exp_gost_10_plus_golova) 
  end
  #*******************************************
  #*******************************************
  # Bedz za pobedu bez primljenog gola
  #*******************************************
  if @home_goals == 0
    @gostujuci_tim_nije_primio_gol = @away_team_season.team.team_badge.primljeno_0_golova + 1
    @away_team_season.team.team_badge.update(:primljeno_0_golova => @gostujuci_tim_nije_primio_gol)
   #******************************************
   # Logika za EXP za gosta da nije primio gol
   #******************************************
   @exp_gost_0_primljeno_golova = @away_team_season.expirience + 600
   @away_team_season.update(:expirience => @exp_gost_0_primljeno_golova)
  end

  if @away_goals == 0
    @domaci_tim_nije_primio_gol = @home_team_season.team.team_badge.primljeno_0_golova + 1
    @home_team_season.team.team_badge.update(:primljeno_0_golova => @domaci_tim_nije_primio_gol)
   #********************************************
   # Logika za EXP za domacin da nije primio gol
   #********************************************
   @exp_domacin_0_primljeno_golova = @home_team_season.expirience + 600
   @home_team_season.update(:expirience => @exp_domacin_0_primljeno_golova)
  end
  #***********************
  #Bedz za 100% efikasnost 
  #***********************
  if @home_shoots == @home_goals
     @bedz_100_posto_domaci = @home_team_season.team.team_badge.sutevi_golovi_sto_posto + 1
     @home_team_season.team.team_badge.update(:sutevi_golovi_sto_posto => @bedz_100_posto_domaci)
   #***********************************************
   # Logika za EXP za sto posto efikasnost domacina
   #***********************************************
     @exp_domaci_100_posto = @home_team_season.expirience + 750
     @home_team_season.update(:expirience => @exp_domaci_100_posto)
  end
  if @away_shoots == @away_goals
     @bedz_100_posto_gost = @away_team_season.team.team_badge.sutevi_golovi_sto_posto + 1
     @away_team_season.team.team_badge.update(:sutevi_golovi_sto_posto => @bedz_100_posto_gost)
   #********************************************
   # Logika za EXP za goste da nije primio gol
   #********************************************
     @exp_gost_100_posto = @away_team_season.expirience + 750
     @away_team_season.update(:expirience => @exp_gost_100_posto)
  end

  #*******************************************
  ############################################################
  # KRAJ LOGIKE ZA BEDZEVE TIMOVA
  ############################################################

  ############################################################
  # LOGIKA ZA BEDZEVE IGRACA
  ############################################################
    
    #**********
    # HAT TRICK
    #**********
    # Domacih igraca
    @home_player_seasons_on_match.each do |home_player_season_on_match|
      @broj_golova_u_nizu_za_domaceg_igraca = home_player_season_on_match[:goals_in_chain]
      if (@broj_golova_u_nizu_za_domaceg_igraca >= 3 && @broj_golova_u_nizu_za_domaceg_igraca != 0)
        @hat_trikova_u_nizu = @broj_golova_u_nizu_za_domaceg_igraca.to_f / 3
        @broj_osvojenih_hat_trick_bedzeva = @hat_trikova_u_nizu.round
        @ukupan_broj_osvojenih_hat_trick_bedzeva = home_player_season_on_match.player.player_badge[:tri_gola_na_utakmici] + @broj_osvojenih_hat_trick_bedzeva 
        home_player_season_on_match.player.player_badge.update(:tri_gola_na_utakmici => @ukupan_broj_osvojenih_hat_trick_bedzeva)
        #***************************************
        # Exp za hat trick bedz domacem igracu
        #***************************************
        @exp_za_hat_trick_domacem_igracu = home_player_season_on_match.expirience + 500
        home_player_season_on_match.update(:expirience => @exp_za_hat_trick_domacem_igracu)
      end
    end

    # Gostujucih igraca
    @away_player_seasons_on_match.each do |away_player_season_on_match|
      @broj_golova_u_nizu_za_gostujuceg_igraca = away_player_season_on_match[:goals_in_chain]
      if (@broj_golova_u_nizu_za_gostujuceg_igraca >= 3 && @broj_golova_u_nizu_za_gostujuceg_igraca != 0)
        @het_trik_u_nizu = @broj_golova_u_nizu_za_gostujuceg_igraca.to_f / 3
        @broj_osvojenih_het_trick_bedzeva = @het_trik_u_nizu.round
        @ukupan_broj_osvojenih_het_trick_bedzeva = away_player_season_on_match.player.player_badge[:tri_gola_na_utakmici] + @broj_osvojenih_het_trick_bedzeva 
        away_player_season_on_match.player.player_badge.update(:tri_gola_na_utakmici => @ukupan_broj_osvojenih_het_trick_bedzeva)
        #***************************************
        # Exp za hat trick bedz gostujuem igracu
        #***************************************
        @exp_za_hat_trick_gostujucem_igracu = away_player_season_on_match.expirience + 500
        away_player_season_on_match.update(:expirience => @exp_za_hat_trick_gostujucem_igracu)
      end
    end
    #****************************************
    # OVDE SE RADI POVECANJE EXP ZA HAT TRICK
    #****************************************
    # FORMULE ZA EXP...
    #********************************************************************************************************#

   #*****************
   # TRI ASISTENCIJE
   #*****************
   # Domacih igraca
   @home_player_seasons_on_match.each do |home_player_season_on_match|
    @broj_asistencija_u_nizu_za_domaceg_igraca = home_player_season_on_match[:assists_in_chain]
    if (@broj_asistencija_u_nizu_za_domaceg_igraca >= 3 && @broj_asistencija_u_nizu_za_domaceg_igraca != 0)
      @tri_asistencije_u_nizu_za_domaceg_igrace = @broj_asistencija_u_nizu_za_domaceg_igraca.to_f / 3
      @broj_ovojennih_bedzeva_za_tri_asistencije = @tri_asistencije_u_nizu_za_domaceg_igrace.round
      @ukupan_broj_osvojenih_bedzeva_za_tri_asistencije = home_player_season_on_match.player.player_badge[:tri_asist_na_utakmici] + @broj_ovojennih_bedzeva_za_tri_asistencije
      home_player_season_on_match.player.player_badge.update(:tri_asist_na_utakmici => @ukupan_broj_osvojenih_bedzeva_za_tri_asistencije)
        #***************************************
        # Exp za tri asistencije bedz domacem igracu
        #***************************************
        @exp_za_tri_asist_domacem_igracu = home_player_season_on_match.expirience + 250
        home_player_season_on_match.update(:expirience => @exp_za_tri_asist_domacem_igracu)
    end
  end

  # Gostujucih igraca
  @away_player_seasons_on_match.each do |away_player_season_on_match|
    @broj_asistencija_u_nizu_za_gostujuceg_igraca = away_player_season_on_match[:assists_in_chain]
    if (@broj_asistencija_u_nizu_za_gostujuceg_igraca >= 3 && @broj_asistencija_u_nizu_za_gostujuceg_igraca != 0)
      @tri_asistencije_u_nizu_za_gostujuceg_igrace = @broj_asistencija_u_nizu_za_gostujuceg_igraca.to_f / 3
      @broj_ovojennih_bedzeva_za_tri_asistencije = @tri_asistencije_u_nizu_za_gostujuceg_igrace.round
      @ukupan_broj_osvojenih_bedzeva_za_tri_asistencije = away_player_season_on_match.player.player_badge[:tri_asist_na_utakmici] + @broj_ovojennih_bedzeva_za_tri_asistencije
      away_player_season_on_match.player.player_badge.update(:tri_asist_na_utakmici => @ukupan_broj_osvojenih_bedzeva_za_tri_asistencije)
        #***************************************
        # Exp za tri asistencije bedz gostu igracu
        #***************************************
        @exp_za_tri_asist_gost_igracu = away_player_season_on_match.expirience + 250
        away_player_season_on_match.update(:expirience => @exp_za_tri_asist_gost_igracu)
    end
  end
  #******************************************************************************************************#

   #*****************
   # TRI ODBRANE
   #*****************
   # Domacih golmana
   @home_player_seasons_on_match.each do |home_player_season_on_match|
    @broj_odbrana_domaceg_golmana_u_nizu = home_player_season_on_match[:gk_saves_in_chain]
    if (@broj_odbrana_domaceg_golmana_u_nizu >= 15 && @broj_odbrana_domaceg_golmana_u_nizu !=0)
      @tri_odbrane_u_nizu_domaceg_golmana = @broj_odbrana_domaceg_golmana_u_nizu.to_f / 15
      @broj_osvojenih_bedzeva_za_tri_odbrane_domaceg_golamana = @tri_odbrane_u_nizu_domaceg_golmana.round
      @ukupan_broj_osvojenih_bedzeva_za_tri_odbrane_domaceg_golmana = home_player_season_on_match.player.player_badge[:tri_odbrane_na_utakmici] + @broj_osvojenih_bedzeva_za_tri_odbrane_domaceg_golamana
      home_player_season_on_match.player.player_badge.update(:tri_odbrane_na_utakmici => @ukupan_broj_osvojenih_bedzeva_za_tri_odbrane_domaceg_golmana)
        #****************************************
        # Exp za tri odbrane bedz domacem golmanu
        #****************************************
        @exp_za_tri_odbrane_domacem_golmanu = home_player_season_on_match.expirience + 1000
        home_player_season_on_match.update(:expirience => @exp_za_tri_odbrane_domacem_golmanu)
    end
   end 

   # Gostujucih golmana
   @away_player_seasons_on_match.each do |away_player_season_on_match|
    @broj_odbrana_gostujuceg_golmana_u_nizu = away_player_season_on_match[:gk_saves_in_chain]
    if (@broj_odbrana_gostujuceg_golmana_u_nizu >= 15 && @broj_odbrana_gostujuceg_golmana_u_nizu !=0)
      @tri_odbrane_u_nizu_gostujuceg_golmana = @broj_odbrana_gostujuceg_golmana_u_nizu.to_f / 15
      @broj_osvojenih_bedzeva_za_tri_odbrane_gostujuceg_golamana = @tri_odbrane_u_nizu_gostujuceg_golmana.round
      @ukupan_broj_osvojenih_bedzeva_za_tri_odbrane_gostujuceg_golmana = away_player_season_on_match.player.player_badge[:tri_odbrane_na_utakmici] + @broj_osvojenih_bedzeva_za_tri_odbrane_gostujuceg_golamana
      away_player_season_on_match.player.player_badge.update(:tri_odbrane_na_utakmici => @ukupan_broj_osvojenih_bedzeva_za_tri_odbrane_gostujuceg_golmana)
        #****************************************
        # Exp za tri odbrane bedz domacem golmanu
        #****************************************
        @exp_za_tri_odbrane_gost_golmanu = away_player_season_on_match.expirience + 1000
        away_player_season_on_match.update(:expirience => @exp_za_tri_odbrane_gost_golmanu)
    end
   end
   #**********************************************************************************************************************************************#   
   #*******************************
   # JEDAN GOL I JEDNA ASISTENCIJA
   #*******************************
   # Domacih igraca
   @home_player_seasons_on_match.each do |home_player_season_on_match|
      if home_player_season_on_match[:goals_in_chain] >= 1 && home_player_season_on_match[:assists_in_chain] >= 1
        # Dodeli tom igracu bedz JEDAN GOL I JEDNA ASISTENCIJA
        @broj_gol_asistencija = home_player_season_on_match.player.player_badge[:gol_asistencija] + 1
        home_player_season_on_match.player.player_badge.update(:gol_asistencija => @broj_gol_asistencija)
        #*****************************
        # Exp za 1 gol i 1 asistenciju
        #*****************************
        @exp_za_gol_asist = home_player_season_on_match.expirience + 300
        home_player_season_on_match.update(:expirience => @exp_za_gol_asist)
          if (home_player_season_on_match[:goals_in_chain] >= 5 && home_player_season_on_match[:assists_in_chain] >= 5)
          # Dodeli tom igracu bedz PET GOLOVA I PET ASISTENCIJA NA MECU
          @broj_gol_asistencija = home_player_season_on_match.player.player_badge[:pet_gol_asistencija] + 1
          home_player_season_on_match.player.player_badge.update(:pet_gol_asistencija => @broj_gol_asistencija)
          end
      end
   end

   # Gostujucih igraca
   @away_player_seasons_on_match.each do |away_player_season_on_match|
      if (away_player_season_on_match[:goals_in_chain] >= 1 && away_player_season_on_match[:assists_in_chain] >= 1)
        # Dodeli tom igracu bedz JEDAN GOL I JEDNA ASISTENCIJA
        @broj_gol_asistencija = away_player_season_on_match.player.player_badge[:gol_asistencija] + 1
        away_player_season_on_match.player.player_badge.update(:gol_asistencija => @broj_gol_asistencija)
        #*****************************
        # Exp za 1 gol i 1 asistenciju
        #*****************************
        @exp_za_gol_asist_away = away_player_season_on_match.expirience + 300
        away_player_season_on_match.update(:expirience => @exp_za_gol_asist_away)
          if (away_player_season_on_match[:goals_in_chain] >= 5 && away_player_season_on_match[:assists_in_chain] >= 5)
          # Dodeli tom igracu bedz PET GOLOVA I PET ASISTENCIJA NA MECU
          @broj_gol_asistencija = away_player_season_on_match.player.player_badge[:pet_gol_asistencija] + 1
          away_player_season_on_match.player.player_badge.update(:pet_gol_asistencija => @broj_gol_asistencija)
          end
      end
   end
   #**********************************************************************************************************************************************#   

   if @match.is_friendly
    #Bedz odigrano 5 prijateljskih za domace igrace
    @home_player_seasons_on_match.each do |home_player_season_on_match|
      @odigrao_prijateljsku_utakmicu = home_player_season_on_match.friendly_matches_played + 1
      home_player_season_on_match.update(:friendly_matches_played => @odigrao_prijateljsku_utakmicu)
      if home_player_season_on_match[:friendly_matches_played] % 5 == 0 && home_player_season_on_match[:friendly_matches_played] != 0 
        @bedz_za_pet_prijateljskih = home_player_season_on_match.player.player_badge.pet_prijateljskih + 1
        home_player_season_on_match.player.player_badge.update(:pet_prijateljskih => @bedz_za_pet_prijateljskih)
        #**********************
        #EXP ZA 5 PRIJATELJSKIH
        #**********************
        @exp_za_5_prjatelj_home = home_player_season_on_match.expirience + 1000
        home_player_season_on_match.update(:expirience => @exp_za_5_prjatelj_home)
      end
    end
    #Bedz odigrano 5 prijateljskih za gostujuce igrace
    @away_player_seasons_on_match.each do |away_player_season_on_match|
      @odigrao_prijateljsku_utakmicu_gost = away_player_season_on_match.friendly_matches_played + 1
      away_player_season_on_match.update(:friendly_matches_played => @odigrao_prijateljsku_utakmicu_gost)
      if away_player_season_on_match[:friendly_matches_played] % 5 == 0 && away_player_season_on_match[:friendly_matches_played] != 0 
        @bedzz_za_pet_prijateljskih = away_player_season_on_match.player.player_badge.pet_prijateljskih + 1
        away_player_season_on_match.player.player_badge.update(:pet_prijateljskih => @bedzz_za_pet_prijateljskih)
        #**********************
        #EXP ZA 5 PRIJATELJSKIH
        #**********************
        @exp_za_5_prjatelj_away = away_player_season_on_match.expirience + 1000
        away_player_season_on_match.update(:expirience => @exp_za_5_prjatelj_away)
      end
    end
   end
  ########################################################################################
  # KRAJ LOGIKE ZA BEDZEVE IGRACA
  ########################################################################################

  #####################################################
  # LOGIKA ZA IS_MATCH_FINISHED
  #####################################################
  @match.update(:is_match_finished => 1)

  ########################################################################################
  # Pocetak logike za kup
  ######################################################################################## 

  if @match.is_tournament
    ###############################
    # Pozicije 1, 2 i kreiranje 9.
    ###############################
     @tournament = Tournament.find_by(:id => @match.tournament.id)
     @match_1 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 1)
     @match_2 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 2)
     if @match_1.is_match_finished && @match_2.is_match_finished && (@match == @match_1 || @match == @match_2 )
        @match_9 = Match.create(:tournament_id => @tournament.id, :tournament_position => 9,
                                :home_team => @match_1.home_team, :away_team => @match_2.away_team, :is_tournament => 1)
        if @match_1.home_goals > @match_1.away_goals
          @match_9.update(:home_team => @match_1.home_team)
        elsif @match_1.away_goals > @match_1.home_goals
          @temp_away_team = HomeTeam.find_by(:id => @match_1.away_team)
          @match_9.update(:home_team => @temp_away_team)
        end
        if @match_2.home_goals > @match_2.away_goals
          @temp_home_team = AwayTeam.find_by(:id => @match_2.home_team)
          @match_9.update(:away_team => @temp_home_team)
        elsif @match_2.away_goals > @match_2.home_goals
          @match_9.update(:away_team => @match_2.away_team)
        end
     end
    ###############################
    # Pozicije 3, 4 i kreiranje 10.
    ###############################

    @match_3 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 3)
     @match_4 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 4)
     if @match_3.is_match_finished && @match_4.is_match_finished && (@match == @match_3 || @match == @match_4 )
        @match_10 = Match.create(:tournament_id => @tournament.id, :tournament_position => 10,
                                :home_team => @match_3.home_team, :away_team => @match_4.away_team, :is_tournament => 1)
        if @match_3.home_goals > @match_3.away_goals
          @match_10.update(:home_team => @match_3.home_team)
        elsif @match_3.away_goals > @match_3.home_goals
          @temp_away1_team = HomeTeam.find_by(:id => @match_3.away_team)
          @match_10.update(:home_team => @temp_away1_team)
        end
        if @match_4.home_goals > @match_4.away_goals
          @temp_home1_team = AwayTeam.find_by(:id => @match_4.home_team)
          @match_10.update(:away_team => @temp_home1_team)
        elsif @match_4.away_goals > @match_4.home_goals
          @match_10.update(:away_team => @match_4.away_team)
        end
     end
    ###############################
    # Pozicije 5, 6 i kreiranje 11.
    ###############################

    @match_5 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 5)
     @match_6 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 6)
     if @match_5.is_match_finished && @match_6.is_match_finished && (@match == @match_5 || @match == @match_6 )
        @match_11 = Match.create(:tournament_id => @tournament.id, :tournament_position => 11,
                                :home_team => @match_5.home_team, :away_team => @match_6.away_team, :is_tournament => 1)
        if @match_5.home_goals > @match_5.away_goals
          @match_11.update(:home_team => @match_5.home_team)
        elsif @match_5.away_goals > @match_5.home_goals
          @temp_away2_team = HomeTeam.find_by(:id => @match_5.away_team)
          @match_11.update(:home_team => @temp_away2_team)
        end
        if @match_6.home_goals > @match_6.away_goals
          @temp_home2_team = AwayTeam.find_by(:id => @match_6.home_team)
          @match_11.update(:away_team => @temp_home2_team)
        elsif @match_6.away_goals > @match_6.home_goals
          @match_11.update(:away_team => @match_6.away_team)
        end
     end

    ###############################
    # Pozicije 7, 8 i kreiranje 12.
    ###############################

    @match_7 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 7)
     @match_8 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 8)
     if @match_7.is_match_finished && @match_8.is_match_finished && (@match == @match_7 || @match == @match_8 )
        @match_12 = Match.create(:tournament_id => @tournament.id, :tournament_position => 12,
                                :home_team => @match_7.home_team, :away_team => @match_8.away_team, :is_tournament => 1)
        if @match_7.home_goals > @match_7.away_goals
          @match_12.update(:home_team => @match_7.home_team)
        elsif @match_7.away_goals > @match_7.home_goals
          @temp_away3_team = HomeTeam.find_by(:id => @match_7.away_team)
          @match_12.update(:home_team => @temp_away3_team)
        end
        if @match_8.home_goals > @match_8.away_goals
          @temp_home3_team = AwayTeam.find_by(:id => @match_8.home_team)
          @match_12.update(:away_team => @temp_home3_team)
        elsif @match_8.away_goals > @match_8.home_goals
          @match_12.update(:away_team => @match_8.away_team)
        end
     end

    ###############################
    # Pozicije 9, 10 i kreiranje 13.
    ###############################

    if@match_1.is_match_finished && @match_2.is_match_finished && @match_3.is_match_finished &&  @match_4.is_match_finished && 
          @match_5.is_match_finished && @match_6.is_match_finished && @match_7.is_match_finished && @match_8.is_match_finished
      @match_9 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 9)
       @match_10 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 10)
       if @match_9.is_match_finished && @match_10.is_match_finished && (@match == @match_9 || @match == @match_10 )
          @match_13 = Match.create(:tournament_id => @tournament.id, :tournament_position => 13,
                                  :home_team => @match_9.home_team, :away_team => @match_10.away_team, :is_tournament => 1)
          if @match_9.home_goals > @match_9.away_goals
            @match_13.update(:home_team => @match_9.home_team)
          elsif @match_9.away_goals > @match_9.home_goals
            @temp_away4_team = HomeTeam.find_by(:id => @match_9.away_team)
            @match_13.update(:home_team => @temp_away4_team)
          end
          if @match_10.home_goals > @match_10.away_goals
            @temp_home4_team = AwayTeam.find_by(:id => @match_10.home_team)
            @match_13.update(:away_team => @temp_home4_team)
          elsif @match_10.away_goals > @match_10.home_goals
            @match_13.update(:away_team => @match_10.away_team)
          end
       end
    end

    #################################
    # Pozicije 11, 12 i kreiranje 14.
    #################################

    if@match_1.is_match_finished && @match_2.is_match_finished && @match_3.is_match_finished &&  @match_4.is_match_finished && 
          @match_5.is_match_finished && @match_6.is_match_finished && @match_7.is_match_finished && @match_8.is_match_finished
      @match_11 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 11)
       @match_12 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 12)
       if @match_11.is_match_finished && @match_12.is_match_finished && (@match == @match_11 || @match == @match_12 )
          @match_14 = Match.create(:tournament_id => @tournament.id, :tournament_position => 14,
                                  :home_team => @match_11.home_team, :away_team => @match_12.away_team, :is_tournament => 1)
          if @match_11.home_goals > @match_11.away_goals
            @match_14.update(:home_team => @match_11.home_team)
          elsif @match_11.away_goals > @match_11.home_goals
            @temp_away5_team = HomeTeam.find_by(:id => @match_11.away_team)
            @match_14.update(:home_team => @temp_away5_team)
          end
          if @match_12.home_goals > @match_12.away_goals
            @temp_home5_team = AwayTeam.find_by(:id => @match_12.home_team)
            @match_14.update(:away_team => @temp_home5_team)
          elsif @match_12.away_goals > @match_12.home_goals
            @match_14.update(:away_team => @match_12.away_team)
          end
        end
     end

    #################################
    # Pozicije 13, 14 i kreiranje 15.
    #################################

    if@match_1.is_match_finished && @match_2.is_match_finished && @match_3.is_match_finished &&  @match_4.is_match_finished && 
            @match_5.is_match_finished && @match_6.is_match_finished && @match_7.is_match_finished && @match_8.is_match_finished
      if @match_9.is_match_finished && @match_10.is_match_finished && @match_11.is_match_finished && @match_12.is_match_finished
        @match_13 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 13)
         @match_14 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 14)
         if @match_13.is_match_finished && @match_14.is_match_finished
            if (@match == @match_13 || @match == @match_14)
                @match_15 = Match.create(:tournament_id => @tournament.id, :tournament_position => 15,
                                        :home_team => @match_13.home_team, :away_team => @match_14.away_team, :is_tournament => 1)
                if @match_13.home_goals > @match_13.away_goals
                  @match_15.update(:home_team => @match_13.home_team)
                elsif @match_13.away_goals > @match_13.home_goals
                  @temp_away6_team = HomeTeam.find_by(:id => @match_13.away_team)
                  @match_15.update(:home_team => @temp_away6_team)
                end
                if @match_14.home_goals > @match_14.away_goals
                  @temp_home6_team = AwayTeam.find_by(:id => @match_14.home_team)
                  @match_15.update(:away_team => @temp_home6_team)
                elsif @match_14.away_goals > @match_14.home_goals
                  @match_15.update(:away_team => @match_14.away_team)
                end
            end
            ################################################
            # Odigravanje 15 utakmice i osvajanje kup bedza.
            ################################################
            @match_15 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 15)
            if (@match == @match_15) && (@match_15.is_match_finished)
              if @match_15.home_goals > @match_15.away_goals
                @dodela_bedza_domcinu_za_osvojen_kup = @match_15.home_team.team.team_badge.osvojen_kup + 1
                @match_15.home_team.team.team_badge.update(:osvojen_kup => @dodela_bedza_domcinu_za_osvojen_kup)
                #********************************************
                # Logika za EXP za domacina da je osvojio kup
                #********************************************
                @dodela_poena_za_domacina_da_je_osvojio_kup = @match_15.home_team.team.team_season.expirience + 2000
                @match_15.home_team.team.team_season.update(:expirience => @dodela_poena_za_domacina_da_je_osvojio_kup)
              elsif @match_15.away_goals > @match_15.home_goals 
                @dodela_bedza_gostu_za_osvojen_kup = @match_15.away_team.team.team_badge.osvojen_kup + 1
                @match_15.away_team.team.team_badge.update(:osvojen_kup => @dodela_bedza_gostu_za_osvojen_kup)
                #********************************************
                # Logika za EXP za gosta da je osvojio kup
                #********************************************
                @dodela_poena_za_gosta_da_je_osvojio_kup = @match_15.away_team.team.team_season.expirience + 2000
                @match_15.away_team.team.team_season.update(:expirience => @dodela_poena_za_gosta_da_je_osvojio_kup)
              end
            end
         end
       end
    end
  end









  ########################################################################################
  # Kraj logike za kup
  ######################################################################################## 
  ############################################################
  ##   Pre gasenja sesije sve CHAIN-ove resetujemo na nulu
  ############################################################
  # Reset chaina za domace igrace
  @home_player_seasons_on_match.each do |home_player_season_on_match|
    home_player_season_on_match.update(:goals_in_chain => 0)
    home_player_season_on_match.update(:assists_in_chain => 0)
    home_player_season_on_match.update(:gk_saves_in_chain => 0)
  end

  # Reset chaina za gostujuce igrace
  @away_player_seasons_on_match.each do |away_player_season_on_match|
  away_player_season_on_match.update(:goals_in_chain => 0)
  away_player_season_on_match.update(:assists_in_chain => 0)
  away_player_season_on_match.update(:gk_saves_in_chain => 0)
  end


    # Do ovde sve treba da bude upisano i sredjeno
  end

  private
  def set_match
    @match = Match.find(params[:id])
  end

 # def match_params
 #   params.require(:match).permit(:match_started, :is_match_started, :is_match_finished)
 # end

  def odredjivanje_levela(parametar)
    if parametar.expirience < 251
        @level = 1
      elsif parametar.expirience > 250 && parametar.expirience < 1501
        @level = 2
      elsif parametar.expirience > 1500 && parametar.expirience < 3501
        @level = 3
      elsif parametar.expirience > 3500 && parametar.expirience < 6001
        @level = 4
      elsif parametar.expirience > 6000 && parametar.expirience < 10001
        @level = 5
      elsif parametar.expirience > 10000 && parametar.expirience < 14001
        @level = 6
      elsif parametar.expirience > 14000 && parametar.expirience < 18001
        @level = 7
      elsif parametar.expirience > 18000 && parametar.expirience < 22001
        @level = 8
      elsif parametar.expirience > 22000 && parametar.expirience < 26001
        @level = 9
      elsif parametar.expirience > 26000 && parametar.expirience < 30001
        @level = 10
      elsif parametar.expirience > 30000 && parametar.expirience < 34001
        @level = 11
      elsif parametar.expirience > 34000 && parametar.expirience < 38001
        @level = 12
      elsif parametar.expirience > 38000 && parametar.expirience < 42001
        @level = 13
      elsif parametar.expirience > 42000 && parametar.expirience < 46001
        @level = 14
      elsif parametar.expirience > 46000 && parametar.expirience < 50001
        @level = 15
      elsif parametar.expirience > 50000 && parametar.expirience < 54001
        @level = 16
      elsif parametar.expirience > 54000 && parametar.expirience < 58001
        @level = 17
      elsif parametar.expirience > 58000 && parametar.expirience < 62001
        @level = 18
      elsif parametar.expirience > 62000 && parametar.expirience < 66001
        @level = 19
      elsif parametar.expirience > 66000 && parametar.expirience < 70001
        @level = 20
      elsif parametar.expirience > 70000 && parametar.expirience < 74001
        @level = 21
      elsif parametar.expirience > 74000 && parametar.expirience < 78001
        @level = 22
      elsif parametar.expirience > 78000 && parametar.expirience < 82001
        @level = 23
      elsif parametar.expirience > 82000 && parametar.expirience < 86001
        @level = 24
      elsif parametar.expirience > 86000 && parametar.expirience < 90001
        @level = 25
      end
  end
end