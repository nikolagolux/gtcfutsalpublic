class ResultsController < ApplicationController
  def index
  end

  # Tabela MATCHES PER LEAGUE
  def show
    # Nije isprobano!
    @league = League.find(params[:id])
    @matches = Match.where(:league_id => @league.id, :is_match_started => 1, :is_match_finished => 1, :season_id => Season.last.id)
  end
end
