class TeamsRangListController < ApplicationController
  
  def index
    @teams = Team.all
    # Ovde trazimo team_sezone
    if @teams != nil
      i = 0
      @teams.each do |team|
        if team.league
          if team.league.is_fiveplusone == true
            @team_season = TeamSeason.where(:team_id => team.id)
            if i == 0 then
              @team_seasons = @team_season
            else 
              @team_seasons.concat(@team_season)
            end
            i = 1
          end
        end
      end
    end

    if @team_seasons
      # Logika za sortiranje timova
      pozicija = 1
      @team_seasons.sort_by{|e| -e[:expirience]}.each do |team_season|
        team_season.update(:position => pozicija)
        pozicija += 1
      end
    end
  end

  def index4plus1
    @teams = Team.all
    # Ovde trazimo team_sezone
    if @teams != nil
      i = 0
      @teams.each do |team|
        if team.league
          if team.league.is_fourplusone == true
            @team_season_4_1 = TeamSeason.where(:team_id => team.id)
            if i == 0 then
              @team_seasons_4_1 = @team_season_4_1
            else 
              @team_seasons_4_1.concat(@team_season_4_1)
            end
            i = 1
          end
        end
      end
    end

    if @team_seasons_4_1 != nil
      # Logika za sortiranje timova
      pozicija = 1
      @team_seasons_4_1.sort_by{|e| -e[:expirience]}.each do |team_season_4_1|
        team_season_4_1.update(:position => pozicija)
        pozicija += 1
      end
    end
  end

  def show
  end

  def update
  end
end
