class LivescoresController < ApplicationController
	before_action :set_league, only: [:show]
  
  def index
  end

  def show
  	@matches = Match.where(:league_id => @league.id, :is_match_finished => 0, :season_id => Season.last.id)
  end


  private 

  def set_league
  	@league = League.find(params[:id])
  end
end
