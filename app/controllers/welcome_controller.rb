class WelcomeController < ApplicationController
  before_action :set_articles, only: [:show, :index]
  # Layout za welcome views, pogledati u folderu views/layouts
  # layout je Rails funkcija
  layout 'application_naslovna'
  before_filter :set_articles

  def index
    @organisation_info = OrganisationInformation.second
  end

  def contact
    @message = Contactmessage.new
    @stadions = Stadion.all
  end 

  def show
  end

  private
  def set_articles
  	#@articles=Article.all
  	@articles = Article.paginate(page: params[:page], per_page: 6 ).order('created_at DESC')
  end
end
