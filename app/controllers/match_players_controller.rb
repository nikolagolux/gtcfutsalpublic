class MatchPlayersController < ApplicationController
  before_action :set_match

  def index
  end

  def new
  end

  def new_home
    # Ovde u @player_season treba da oznacimo sve igrace iz HOME tima (koji nisu suspendovani)
    @home_team = @this_match.home_team.team
    @players = Player.where(:team_id => @home_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      player_season = PlayerSeason.where(:player_id => player.id, :season_id => Season.last.id)
      player_season_is_suspended = PlayerSeason.find_by(:player_id => player.id, :season_id => Season.last.id).is_suspended
      unless player_season_is_suspended
        if i == 0 then
          @player_seasons = player_season
        else 
          @player_seasons.concat(player_season)
        end
        i = 1
      end
    end
    current_match.update(:is_home_team_players_chosen => 1)
    #@player_seasons = PlayerSeason.where(:player_id => )
    session[:is_home] = true
    #@player = Player.where(:id => :player_id)
  end

  def new_away
    # Ovde u @player_season treba da oznacimo sve igrace iz AWAY tima
    @away_team = @this_match.away_team.team
    @players = Player.where(:team_id => @away_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      player_season = PlayerSeason.where(:player_id => player.id, :season_id => Season.last.id)
      player_season_is_suspended = PlayerSeason.find_by(:player_id => player.id, :season_id => Season.last.id).is_suspended
      unless player_season_is_suspended
        if i == 0 then
          @player_seasons = player_season
        else
          @player_seasons.concat(player_season)
        end
        i = 1
      end
    end
    current_match.update(:is_away_team_players_chosen => 1)
    #@player_seasons = PlayerSeason.where(:player_id => )
    session[:is_home] = false
    #@player = Player.where(:id => :player_id)
  end

  def create
  end

  def chooseplayers
    # ovde su svi id-evi igraca koji su izabrani u checkbox-ovima
    # da su dosli na utakmicu
    params[:player_season_ids].each do|player_season_id| 
      MatchPlayer.create(
        :player_season_id => player_season_id,
        :match_id => @this_match.id,
        :is_home => session[:is_home]
      )
    end
    redirect_to match_path(@this_match)
  end

  def edit
  end

  def update
  end

  def show
  end

  private
  def set_match
    #session[:this_match] = current_match #Match.find(params[:id])
    #@this_match = current_match
    @this_match = current_match
  end

end
