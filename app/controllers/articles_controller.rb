class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  #before_action :require_user, except: [:index, :show]
  protect_from_forgery with: :null_session,
      if: Proc.new { |c| c.request.format =~ %r{application/json} }
  skip_before_action :verify_authenticity_token


  
  # Oznaceni svi artikli
  def index
    @articles = Article.where(:is_general_article => true).paginate(page: params[:page], per_page: 9 ).order('created_at DESC')
    #@artikli = Article.all
    #render :json => @artikli
    # U kontroleru moze da se pravi novi red u tabeli
    #Player.create(:name => "ProbnoIme", :surname => "Probno prezime", :JMBG => "22334455", :team_id => 1)
  end

  def show
  end

  def new
    @article = Article.new
  end

  def edit
  end

  def create
    @article = Article.new
    # Artikl (clanak) dodeljujemo useru
    if @article.save
      format.json { render json: @article, status: :created }
      format.xml { render xml: @article, status: :created }
      redirect_to root_path
    end
  end

  def update
    if @article.update(article_params)
      flash[:success] = "Article was successfully updated"
      redirect_to articles_path
    else
      redirect_to articles_path
    end
  end

  # Delete article
  def destroy
    @article.destroy
    flash[:success] = "Article was successfully destroied"
    redirect_to articles_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :description, :avatar)
    end
    
    #def require_same_user
      #if current_user != @user
        #flash[:danger] = "You can only edit your own account"
        #redirect_to root_path
      #end
   #end
    
end
