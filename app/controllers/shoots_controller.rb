class ShootsController < ApplicationController
	before_action :is_match_started


	def index
		@shoots = Shoot.all
	end

	def new_home
		#************************************************************
	    # Sutevi ne mogu da se kreiraju ako match nije startovan
	    #************************************************************
	    if current_match.is_match_started
			@shoot = Shoot.new

			@home_team = current_match.home_team.team
			@home_team_season = TeamSeason.find_by(:team_id => current_match.home_team.team.id, :season_id => Season.last.id)
			#@home_team_season = TeamSeason.where(:team_id => @home_team.id)
			session[:is_home] = true
		    
		    @shoot.match_id = current_match.id

			# Pitamo se da li je faul dao domacin ili gost
			@pauza = (current_match.second_half_started - current_match.first_half_ended)
			@shoot_scored_m = (DateTime.now.seconds_since_midnight - @pauza - current_match.match_started).to_f / 60

			@shoot.created_at = @shoot_scored_m.round

			@shoot.is_home = session[:is_home]

			if(@shoot.is_home)
				@shoot.team_season_id = @home_team_season.id
			else
				@shoot.team_season_id = @away_team_season.id
			end

		    if @shoot.save
		      flash[:success] = "Shoot added!"
		      redirect_to match_path(current_match)
		    else
		      render 'new'
		    end
		end
	end

	def new_away
		#************************************************************
	    # Sutevi ne mogu da se kreiraju ako match nije startovan
	    #************************************************************
	    if current_match.is_match_started
			@shoot = Shoot.new

			@away_team = current_match.away_team.team
			@away_team_season = TeamSeason.find_by(:team_id => current_match.away_team.team.id, :season_id => Season.last.id)
			#@away_team_season = TeamSeason.where(:team_id => @away_team.id)
			session[:is_home] = false

			@shoot.match_id = current_match.id

			# Pitamo se da li je faul dao domacin ili gost
			@pauza = (current_match.second_half_started - current_match.first_half_ended)
			@shoot_scored_m = (DateTime.now.seconds_since_midnight - @pauza - current_match.match_started).to_f / 60

			@shoot.created_at = @shoot_scored_m.round

			@shoot.is_home = session[:is_home]

			if(@shoot.is_home)
				@shoot.team_season_id = @home_team_season.id
			else
				@shoot.team_season_id = @away_team_season.id
			end

		    if @shoot.save
		      flash[:success] = "Shoot added!"
		      redirect_to match_path(current_match)
		    else
		      render 'new'
		    end	
		end
	end
end