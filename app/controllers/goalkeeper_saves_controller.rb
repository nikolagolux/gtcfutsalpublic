class GoalkeeperSavesController < ApplicationController
  before_action :is_match_started
  
  def index
    @goalkeeper_saves = GoalkeeperSave.all 
  end

  def new
    # @foul = Foul.new
    # @player_seasons = PlayerSeason.all
    # @player = Player.where(:id => :player_id)
  end

  def new_home
    @goalkeeper_save = GoalkeeperSave.new
    # Ovde u @player_season treba da oznacimo sve igrace iz HOME tima
    #@player = Player.where(:id => :player_id)
    # Ovde u @player_season treba da oznacimo sve igrace iz HOME tima
    @home_team = current_match.home_team.team
    @players = Player.where(:team_id => @home_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      @player_season = PlayerSeason.where(:player_id => player.id)
      if i == 0 then
        @player_seasons = @player_season
      else 
        @player_seasons.concat(@player_season)
      end
      i = 1
    end
    session[:is_home] = true
    # U promenljivu match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @match_players = MatchPlayer.where(
      :match_id => current_match.id,
      :is_home => session[:is_home])

    # Ovde trazimo player_sezone za izabrane igrace,
    # glavna promenljiva je player_seasons_on_match
    i = 0
    @match_players.each do |match_player|
      @player_season_on_match = PlayerSeason.where(:id => match_player.player_season_id)
      if i == 0 then
        @player_seasons_on_match = @player_season_on_match
      else
        @player_seasons_on_match.concat(@player_season_on_match)
      end
      i = 1
    end
  end

  def new_away
    @goalkeeper_save = GoalkeeperSave.new
    # Ovde u @player_season treba da oznacimo sve igrace iz AWAY tima
    # @team = Team.find() ...
    # @player = Player.where(:team_id => self.team)
    @away_team = current_match.away_team.team
    @players = Player.where(:team_id => @away_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      @player_season = PlayerSeason.where(:player_id => player.id)
      if i == 0 then
        @player_seasons = @player_season
      else 
        @player_seasons.concat(@player_season)
      end
      i = 1
    end
    session[:is_home] = false
    # U promenljivu match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @match_players = MatchPlayer.where(
      :match_id => current_match.id,
      :is_home => session[:is_home])

    # Ovde trazimo player_sezone za izabrane igrace,
    # glavna promenljiva je player_seasons_on_match
    i = 0
    @match_players.each do |match_player|
      @player_season_on_match = PlayerSeason.where(:id => match_player.player_season_id)
      if i == 0 then
        @player_seasons_on_match = @player_season_on_match
      else
        @player_seasons_on_match.concat(@player_season_on_match)
      end
      i = 1
    end 
  end

  def create
    #************************************************************
    # Odbrane ne mogu da se kreiraju ako match nije startovan
    #************************************************************
    if current_match.is_match_started
     @goalkeeper_save = GoalkeeperSave.new(goalkeeper_saves_params)
     # Dodeljujemo datom faul da je dat na odredjenoj utakmici
     @goalkeeper_save.match_id = current_match.id
     # Pitamo se da li je faul dao domacin ili gost
     @pauza = (current_match.second_half_started - current_match.first_half_ended)
     @goalkeeper_save_scored_m = (DateTime.now.seconds_since_midnight - @pauza - current_match.match_started).to_f / 60

     @goalkeeper_save.created_at = @goalkeeper_save_scored_m.round

     # Pitamo se da li je gol dao domacin ili gost
      @goalkeeper_save.is_home = session[:is_home]

      # Bodovanje igraca se vrsi po vaznosti utakmice V i jacini ekipe J
      # Bodovanje igraca je zbir Q+W+E+R+T
      # Dodajemo bodove datom igracu za postignut gol (Q)
      # Prvo trazimo igraca koji je dao gol
      @player_season = PlayerSeason.find_by(:id => @goalkeeper_save.player_season.id)

     # Zatim gledamo da li je mec 1) prijateljski; 2) drugoligaski, 3) kup, 4) prvoligaski
      # 1) prijateljski mec, V = 1
      if @goalkeeper_save.match.is_friendly
        @v = 1
      # 2) drugoligaski mec, V = 2.5
      elsif @goalkeeper_save.match.is_second_league
        @v = 2.5
      # 3) kup, V = 3.0
      elsif @goalkeeper_save.match.is_tournament
        @v = 3.0
      # 4) prvoligaski mec, V = 4.0
      elsif @goalkeeper_save.match.is_first_league
        @v = 4.0
      end

      # Trazimo ukupan broj 5+1 ekipa
    # i ukupan broj 4+1 ekipa
    @svi_timovi = Team.all
    # Ovde trazimo team_sezone
    i = 0
    j = 0
    @svi_timovi.each do |team|
      if team.league != nil
        if team.league.is_fiveplusone == true
          @team_season_5_1 = TeamSeason.where(:team_id => team.id, :season_id => Season.last.id)
          if i == 0 then
            @team_seasons_5_1 = @team_season_5_1
          else 
            @team_seasons_5_1.concat(@team_season_5_1)
          end
          i = 1
        elsif team.league.is_fourplusone == true
          @team_season_4_1 = TeamSeason.where(:team_id => team.id, :season_id => Season.last.id)
          if j == 0 then
            @team_seasons_4_1 = @team_season_4_1
          else 
            @team_seasons_4_1.concat(@team_season_4_1)
          end
          j = 1
        end
      end
    end

    # Broj 5+1 ekipa
    if @team_seasons_5_1 != nil
      @ukupno_ekipa_5_1 = @team_seasons_5_1.count
    else
      @ukupno_ekipa_5_1 = 0
    end

    # Broj 4+1 ekipa
    if @team_seasons_4_1 != nil
      @ukupno_ekipa_4_1 = @team_seasons_4_1.count
    else
      @ukupno_ekipa_4_1 = 0
    end

    # Minimalna jacina tima
    @minimalna_jacina_tima_5_1 = 15
    @minimalna_jacina_tima_4_1 = 15

     ###########################
    # Jacina protivnickog tima
    ###########################
    # Definisan home team
    @home_team_season = TeamSeason.find_by(:team_id => current_match.home_team.team.id, :season_id => Season.last.id)
    @home_team = current_match.home_team.team
    # @home_team_season = @home_team_season
    # Definisan away team
    @away_team_season = TeamSeason.find_by(:team_id => current_match.away_team.team.id, :season_id => Season.last.id)
    @away_team = current_match.away_team.team
    # @away_team_season = @away_team_season
    #############################
    # Racunamo jacinu HOME tima
    #############################
    # Za prvoplasiranu i poslednjih 15 ekipa postoje drugi kriterijumi za bodovanje
    # LIGA 4+1
    if @home_team.league.is_fourplusone
      if @home_team_season.position == nil || @home_team_season.position == 0
        @home_team_season.position = 0
        @jacina_domaceg_tima = @minimalna_jacina_tima_4_1
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @home_team_season.position == 1
        @jacina_domaceg_tima = @ukupno_ekipa_4_1
      # Standardna formula za racunanje bodova
      elsif @home_team_season.position > 1 && @home_team_season.position < (@ukupno_ekipa_4_1 - 14)
        @jacina_domaceg_tima = @ukupno_ekipa_4_1 - @home_team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @home_team_season.position > (@ukupno_ekipa_4_1 - 15)
        @jacina_domaceg_tima = @minimalna_jacina_tima_4_1
      end
    # LIGA 5+1
    elsif @home_team.league.is_fiveplusone
      if @home_team_season.position == nil || @home_team_season.position == 0
        @home_team_season.position = 0
        @jacina_domaceg_tima = @minimalna_jacina_tima_5_1
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @home_team_season.position == 1
        @jacina_domaceg_tima = @ukupno_ekipa_5_1
      # Standardna formula za racunanje bodova
      elsif @home_team_season.position > 1 && @home_team_season.position < (@ukupno_ekipa_5_1 - 14)
        @jacina_domaceg_tima = @ukupno_ekipa_5_1 - @home_team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @home_team_season.position > (@ukupno_ekipa_5_1 - 15)
        @jacina_domaceg_tima = @minimalna_jacina_tima_5_1
      end
    end
    @j = @jacina_domaceg_tima

    #############################
    # Racunamo jacinu AWAY tima
    #############################
        # Za prvoplasiranu i poslednjih 15 ekipa postoje drugi kriterijumi za bodovanje
    # LIGA 4+1
    if @away_team.league.is_fourplusone
      if @away_team_season.position == nil || @away_team_season.position == 0
        @away_team_season.position = 0
        @jacina_gostujuceg_tima = @minimalna_jacina_tima_4_1
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @away_team_season.position == 1
        @jacina_gostujuceg_tima = @ukupno_ekipa_4_1
      # Standardna formula za racunanje bodova
      elsif @away_team_season.position > 1 && @away_team_season.position < (@ukupno_ekipa_4_1 - 14)
        @jacina_gostujuceg_tima = @ukupno_ekipa_4_1 - @away_team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @away_team_season.position > (@ukupno_ekipa_4_1 - 15)
        @jacina_gostujuceg_tima = @minimalna_jacina_tima_4_1
      end
    # LIGA 5+1
    elsif @away_team.league.is_fiveplusone
      if @away_team_season.position == nil  || @away_team_season.position == 0
        @away_team_season.position = 0
        @jacina_gostujuceg_tima = @minimalna_jacina_tima_5_1
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @away_team_season.position == 1
        @jacina_gostujuceg_tima = @ukupno_ekipa_5_1
      # Standardna formula za racunanje bodova
      elsif @away_team_season.position > 1 && @away_team_season.position < (@ukupno_ekipa_5_1 - 14)
        @jacina_gostujuceg_tima = @ukupno_ekipa_5_1 - @away_team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @away_team_season.position > (@ukupno_ekipa_5_1 - 15)
        @jacina_gostujuceg_tima = @minimalna_jacina_tima_5_1
      end
    end
    @j = @jacina_gostujuceg_tima

      # Bodovi za gol se proracunavaju po formuli 1*V*J*3 (prva jedinica predstavlja broj golova - BG)
      @bodovi_za_odbranu = @v*@j*0.5
      @bodovi_za_odbranu.round
      @bodovi_za_odbranu += @player_season.expirience
      @player_season.update(:expirience => @bodovi_za_odbranu)

      # Odredjivanje levela igraca u odnosu na osvojene bodove
      if @player_season.expirience < 251
        @level = 1
      elsif @player_season.expirience > 250 && @player_season.expirience < 1501
        @level = 2
      elsif @player_season.expirience > 1500 && @player_season.expirience < 3501
        @level = 3
      elsif @player_season.expirience > 3500 && @player_season.expirience < 6001
        @level = 4
      elsif @player_season.expirience > 6000 && @player_season.expirience < 10001
        @level = 5
      elsif @player_season.expirience > 10000 && @player_season.expirience < 14001
        @level = 6
      elsif @player_season.expirience > 14000 && @player_season.expirience < 18001
        @level = 7
      elsif @player_season.expirience > 18000 && @player_season.expirience < 22001
        @level = 8
      elsif @player_season.expirience > 22000 && @player_season.expirience < 26001
        @level = 9
      elsif @player_season.expirience > 26000 && @player_season.expirience < 30001
        @level = 10
      elsif @player_season.expirience > 30000 && @player_season.expirience < 34001
        @level = 11
      elsif @player_season.expirience > 34000 && @player_season.expirience < 38001
        @level = 12
      elsif @player_season.expirience > 38000 && @player_season.expirience < 42001
        @level = 13
      elsif @player_season.expirience > 42000 && @player_season.expirience < 46001
        @level = 14
      elsif @player_season.expirience > 46000 && @player_season.expirience < 50001
        @level = 15
      elsif @player_season.expirience > 50000 && @player_season.expirience < 54001
        @level = 16
      elsif @player_season.expirience > 54000 && @player_season.expirience < 58001
        @level = 17
      elsif @player_season.expirience > 58000 && @player_season.expirience < 62001
        @level = 18
      elsif @player_season.expirience > 62000 && @player_season.expirience < 66001
        @level = 19
      elsif @player_season.expirience > 66000 && @player_season.expirience < 70001
        @level = 20
      elsif @player_season.expirience > 70000 && @player_season.expirience < 74001
        @level = 21
      elsif @player_season.expirience > 74000 && @player_season.expirience < 78001
        @level = 22
      elsif @player_season.expirience > 78000 && @player_season.expirience < 82001
        @level = 23
      elsif @player_season.expirience > 82000 && @player_season.expirience < 86001
        @level = 24
      elsif @player_season.expirience > 86000 && @player_season.expirience < 90001
        @level = 25
      end

      @player_season.update(:level => @level)

     
     @goalkeeper_save.is_home = session[:is_home]
      if @goalkeeper_save.save
        flash[:success] = "GK save added!"
        redirect_to match_path(current_match)
      else
        render 'new'
      end
    end
  end

  def edit
  end

  def update
  end

  def show
  end

  private
  def goalkeeper_saves_params
    params.require(:goalkeeper_save).permit( :match_id, :player_season_id, :is_home)
  end
end
