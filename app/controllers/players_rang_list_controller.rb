class PlayersRangListController < ApplicationController

  def index
    @players = Player.all

    #i = 0
    #@players.each do |player|
    #  @player_season = PlayerSeason.where(:player_id => player.id)
    #  if i == 0 then
    #    @player_seasons = @player_season
    #  else 
    #    @player_seasons.concat(@player_season)
    #  end
    #  i = 1
    #end
    if @players != nil
      j = 0
      @players.each do |player|
        if player.team
          if player.team.league != nil
            if player.team.league.is_fiveplusone == true
              @player_season_5_1 = PlayerSeason.where(:player_id => player.id)
              if j == 0 then
                @player_seasons_5_1 = @player_season_5_1
              else 
                @player_seasons_5_1.concat(@player_season_5_1)
              end
              j = 1
            end
          end
        end
      end

      # sada imamo sve @player_seasons_5_1
      if @player_seasons_5_1 != nil
        # Logika za sortiranje igraca
        pozicija = 1
        @player_seasons_5_1.sort_by{|e| -e[:expirience]}.each do |player_season_5_1|
          player_season_5_1.update(:position => pozicija)
          pozicija += 1
        end
      end
    end
  end

  def index4plus1
    @players = Player.all
    if @players != nil
      i = 0
      @players.each do |player|
        if player.team
          if player.team.league != nil
            if player.team.league.is_fourplusone == true
              @player_season_4_1 = PlayerSeason.where(:player_id => player.id)
              if i == 0 then
                @player_seasons_4_1 = @player_season_4_1
              else 
                @player_seasons_4_1.concat(@player_season_4_1)
              end
              i = 1
            end
          end
        end
      end

      if @player_seasons_4_1 != nil
        pozicija_4_1 = 1
        @player_seasons_4_1.sort_by{|e| -e[:expirience]}.each do |player_season_4_1|
          player_season_4_1.update(:position => pozicija_4_1)
          pozicija_4_1 += 1
        end
      end
    end
  end

  def show
  end

  def update
  end
end
