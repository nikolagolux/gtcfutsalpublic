class DelegateSerializer < ActiveModel::Serializer
  attributes :id, :name, :surname, :avatar_url
end
