class TeamSerializer < ActiveModel::Serializer
  attributes :id, :name, :avatar_url, :league_id, :tournament_id, :team_season_id, :team_badge_id
end
