class UserSerializer < ActiveModel::Serializer
  attributes :id, :player_id, :delegate_id
end
