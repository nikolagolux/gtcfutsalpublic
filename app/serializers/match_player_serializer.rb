class MatchPlayerSerializer < ActiveModel::Serializer
  attributes :id, :player_season_id, :match_id, :is_home, 
  			 :player_avatar_url, :player_name, :player_surname, :player_id,
  			 :player_goals, :player_assists, :player_y_c, :player_r_c, :player_gk_saves
end