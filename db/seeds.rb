#####################################################
# ADMINS
#####################################################
Admin.create!([
	# Barselona
	{name: "admin1", surname: "gtcsport" },
	{name: "admin2", surname: "gtcsport" }
])

#####################################################
# SEASON
#####################################################
Season.create!([
  {name: "2016/2017"}
])

#####################################################
# STADION
#####################################################
10.times do |counter| 
	Stadion.create!([
	  {name: "Stadium-#{counter+1}", address: "Address-#{counter+1}"}
	])
end

#####################################################
# REFEREE
#####################################################
10.times do |counter| 
	Referee.create!([
	  {name: "Referee-#{counter+1}"}
	])
end

#####################################################
# LEAGUES
#####################################################
League.create!([
	# Lige 5+1
	{name: "First League", is_fourplusone: false, is_fiveplusone: true},
	{name: "Second League", is_fourplusone: false, is_fiveplusone: true},
	{name: "U19 League", is_fourplusone: false, is_fiveplusone: true},
	{name: "U16 League", is_fourplusone: false, is_fiveplusone: true},
	{name: "U13 League", is_fourplusone: false, is_fiveplusone: true},
	# Lige 4+1
	{name: "First League 4+1", is_fourplusone: true, is_fiveplusone: false},
	{name: "GTC League 4+1", is_fourplusone: true, is_fiveplusone: false},
	{name: "Business League 4+1", is_fourplusone: true, is_fiveplusone: false},
	{name: "W2 League 4+1", is_fourplusone: true, is_fiveplusone: false},
	{name: "IT League 4+1", is_fourplusone: true, is_fiveplusone: false}
])

#####################################################
# TEAMS
#####################################################
# Inace se automatski prave TEAM SEASONS i
# TEAM BADGES, medjutim, ovde moramo sami da
# pravimo
#####################################################
Team.create!([
	# Ekipe Prva liga 5+1
	{name: "Warriors", league_id: 1 },
	{name: "Storm", league_id: 1 },
	{name: "Wolfs", league_id: 1 },
	{name: "Tigers", league_id: 1 },
	{name: "Rangers", league_id: 1 }
	#{name: "Eagles", league_id: 1 },
	# Ekipe Druga liga 5+1
	#{name: "Crvena Zvezda" },
	#{name: "Partizan" },
	#{name: "Zemun" },
	# Ekipe Treca liga 5+1
	#{name: "Radnicki" },
	#{name: "OFK Beograd" },
	#{name: "Zmaj" }
])

@brojac_timova = 0
10.times do |counter| 
	Team.create!([
		{name: "Team-#{@brojac_timova+1}", league_id: "#{counter}" },
		{name: "Team-#{@brojac_timova+2}", league_id: "#{counter}" },
		{name: "Team-#{@brojac_timova+3}", league_id: "#{counter}" },
		{name: "Team-#{@brojac_timova+4}", league_id: "#{counter}" },
		{name: "Team-#{@brojac_timova+5}", league_id: "#{counter}" },
		{name: "Team-#{@brojac_timova+6}", league_id: "#{counter}" },
		{name: "Team-#{@brojac_timova+7}", league_id: "#{counter}" },
		{name: "Team-#{@brojac_timova+8}", league_id: "#{counter}" }
	])
	@brojac_timova += 8
end
	

#####################################################
# LANGUAGES
#####################################################
Language.create!([
	###############
	# 1. jezik
	# Engleski
	###############
	{
	 unesite_email: "Enter email",
	 unesite_sifru: "Enter password",
	 brojac_igraca: "Players", 
	 brojac_timova: "Teams",
	 brojac_balona: "Stadiums", 
	 brojac_liga: 	"Leagues",
	 meni_vesti: "News", 
	 meni_info: "Info",
	 meni_kup: "Cup", 
	 meni_pravila: "Rules",
	 meni_registruj_ekipu: "Register Team", 
	 meni_kontakt: "Contact",
	 meni_tereni: "Stadiums", 
	 meni_sudije: "Referees",
	 meni_tabele: "Tables", 
	 meni_statistike: "Stats",
	 meni_rezultati: "Results", 
	 meni_uzivo_utakmice: "Live scores",
	 meni_stranice_liga: "Leagues pages", 
	 meni_rang_lista_ekipa: "Teams rang list",
	 meni_rang_lista_igraca: "Players rang list",
	 meni_kup_5_plus_1: "Cup 5+1", 
	 meni_kup_4_plus_1: "Cup 4+1",
	 meni_pravilnik_5_plus_1: "Rules 5+1", 
	 meni_pravilnik_4_plus_1: "Rules 4+1",
	 footer_prijavi_se: "Register", 
	 footer_novi_delegat: "New delegate",
	 pozicija: "Position", 
	 pozicija_skraceno: "Pos",
	 ekipa: "Team", 
	 ekipa_skraceno: "T",
	 igrac: "Player", 
	 igrac_skraceno: "P",
	 golova: "Goals", 
	 golova_skraceno: "G",
	 asistencija: "Assists",
	 asistencija_skraceno: "A",
	 zutih_kartona: "Yellow Cards", 
	 zutih_kartona_skraceno: "YC",
	 crvenih_kartona: "Red Cards", 
	 crvenih_kartona_skraceno: "RC",
	 nastupa: "Game Played", 
	 nastupa_skraceno: "GP",
	 pobeda: "Games Win", 
	 pobeda_skraceno: "GW",
	 neresenih: "Games Draw",
	 neresenih_skraceno: "GD",
	 izgubljenih: "Games Loss",
	 izgubljenih_skraceno: "GL",
	 dato_golova: "Goals Scored",
	 dato_golova_skraceno: "GS",
	 gol_razlika: "Gol razlika",
	 gol_razlika_skraceno: "GR",
	 bodova: "Points",
	 bodova_skraceno: "Pts",
	 datum: "Date",
	 vreme: "Time",
	 domacin: "Home",
	 domacin_skraceno: "H",
	 rezultat: "Score",
	 rezultat_skraceno: "Score",
	 gost: "Away",
	 gost_skraceno: "A",
	 liga: "League",
	 liga_skraceno: "L",
	 sezona: "Season",
	 sezona_skraceno: "S",
	 teren: "Stadium",
	 teren_skraceno: "S",
	 primljeno_golova:"Goals received",
	 primljeno_golova_skraceno:"GR"
	 },

	###############
	# 2. jezik
	# Srpski
	###############
	{
	 unesite_email: "Unesite email",
	 unesite_sifru: "Unesite šifru",
	 brojac_igraca: "Igračai", 
	 brojac_timova: "Timovi",
	 brojac_balona: "Baloni", 
	 brojac_liga: 	"Lige",
	 meni_vesti: "Vesti", 
	 meni_info: "Info",
	 meni_kup: "Kup", 
	 meni_pravila: "Pravila",
	 meni_registruj_ekipu: "Registruj ekipu", 
	 meni_kontakt: "Kontakt",
	 meni_tereni: "Baloni", 
	 meni_sudije: "Sudije",
	 meni_tabele: "Tabele", 
	 meni_statistike: "Statistike",
	 meni_rezultati: "Rezultati", 
	 meni_uzivo_utakmice: "Uživo utakmice",
	 meni_stranice_liga: "Lige", 
	 meni_rang_lista_ekipa: "Rang lista ekipa",
	 meni_rang_lista_igraca: "Rang lista igrača",
	 meni_kup_5_plus_1: "Kup 5+1", 
	 meni_kup_4_plus_1: "Kup 4+1",
	 meni_pravilnik_5_plus_1: "Pravilnik 5+1", 
	 meni_pravilnik_4_plus_1: "Pravilnik 4+1",
	 footer_prijavi_se: "Prijavi se", 
	 footer_novi_delegat: "Novi delegat",
	 pozicija: "Pozicija", 
	 pozicija_skraceno: "Poz",
	 ekipa: "Ekipe", 
	 ekipa_skraceno: "E",
	 igrac: "Igrač", 
	 igrac_skraceno: "I",
	 golova: "Golova", 
	 golova_skraceno: "G",
	 asistencija: "Asistencija",
	 asistencija_skraceno: "A",
	 zutih_kartona: "Žutih kartona", 
	 zutih_kartona_skraceno: "ŽK",
	 crvenih_kartona: "Crvenih kartona", 
	 crvenih_kartona_skraceno: "CK",
	 nastupa: "Nastupa", 
	 nastupa_skraceno: "Nas",
	 pobeda: "Pobeda", 
	 pobeda_skraceno: "Pob",
	 neresenih: "Nerešenih",
	 neresenih_skraceno: "Ner",
	 izgubljenih: "Izgubljenih",
	 izgubljenih_skraceno: "Izg",
	 dato_golova: "Dato golova",
	 dato_golova_skraceno: "DG",
	 gol_razlika: "Gol razlika",
	 gol_razlika_skraceno: "GR",
	 bodova: "Bodova",
	 bodova_skraceno: "Bod",
	 datum: "Datum",
	 vreme: "Vreme",
	 domacin: "Domaćin",
	 domacin_skraceno: "Dom",
	 rezultat: "Rezultat",
	 rezultat_skraceno: "Rez",
	 gost: "Gost",
	 gost_skraceno: "G",
	 liga: "Liga",
	 liga_skraceno: "Lig",
	 sezona: "Sezona",
	 sezona_skraceno: "Sez",
	 teren: "Balon",
	 teren_skraceno: "B",
	 primljeno_golova:"Primljeno golova",
	 primljeno_golova_skraceno:"PG"
	 }
])
#####################################################
# L2ANGUAGES (LANGUAGES 2)
#####################################################
L2anguage.create!([
	###############
	# 1. jezik
	# Engleski
	###############
	{
	 dodaj_sut:"Add kick",
	 dodaj_igrace:"",
	 predstojeci_mecevi:"Upcoming matches",
	 izaberi:"Choose",
	 poruke:"Message",
	 player_stats:"Player stats",
	 player_bedzevi:"Player badges",
	 player_team:"Player's Team",
	 petnaest_odbrana_na_utakmici:"15 defenses at match",
	 broj_pobeda:"Number of wins",
	 broj_neresenih:"Number of draws",
	 broj_izgubljenih:"Number of lost",
	 email:"email",
	 unesi_prezime_kapitena:"Enter a surname of the captain",
	 izmeni_podatke_za_ekipu:"Edit team information",
	 podesavanje_igraca:"Edit players",
	 unesi_ime_kapitena:"Enter a name of the captain",
	 svi_igraci:"All players",
	 registruj_igraca:"Registrate a player",
	 korisnicko_ime:"Username",
	 datum_rodjenja:"Birthdate",
	 odigranih_meceva:"Played matches",
	 asistencije:"Assistances",
	 golovi:"Goals",
	 faulovi:"Fouls",
	 zuti_kartoni:"Yellow cards",
	 crveni_kartoni:"Red cards",
	 tim:"Team",
	 unesi_naziv_sezone:"Enter a name of the season",
	 registracija_tima_sa_8_igraca:"Register team with 8 players",
	 registracija_tima_sa_10_igraca:"Register team with 10 players",
	 registracija_tima_sa_12_igraca:"Register team with 12 players",
	 registracija_tima_sa_14_igraca:"Register team with 14 players",
	 registracija_tima_sa_16_igraca:"Register team with 16 players",
	 sve_ekipe:"All teams",
	 ime_ekipe:"Team name",
	 unesi_ime_ekipe:"Enter team name",
	 kapiten:"Captain",
	 unesi_ime_igraca:"Enter player's name",
	 unesi_prezime_igraca:"Enter surname",
	 unesi_email_igraca:"Enter email",
	 unesi_lozinku_igraca:"Enter password",
	 registracioni_token:"Registration token",
	 team_stats:"Team stats",
	 team_badges:"Team badges",
	 igraci:"Players",
	 rang_lista:"Rang List",
	 ukucaj_poruku:"Type your message",
	 share:"Share",
	 your_conversation:"Your conversation",
	 toggle_navigation:"Toggle navigation",
	 pratite_nas:"Follow us",
	 godina:"year",
	 potvrdi:"Confirm",
	 azuriraj_nalog:"Update account",
	 prepiska_sa:"Conversation with",
	 restore:"Restore",
	 poruka:"Message",
	 are_you_sure:"Are you sure?",
	 primljene_poruke:"Inbox",
	 poslate_poruke:"Sent message",
	 kanta:"Trash",
	 isprazni_kantu:"Empty trash",
	 posalji:"Send",
	 podesavanja_naloga: "Account settings",
	 informacije_o_meni: "Info",
	 moj_profil: "My Account",
	 kalendar_takmicenja: "Calendar",
	 my_articles: "My Article",
	 log_out: "Log Out",
	 created: "Created",
	 create_article: "Create an article",
	 edit_this_article: "Edit",
	 delete_this_article: "Delete",
	 delete_article_confirmation: "Are you sure you want to delete this article?",
	 naslov: "Title",
	 sadrzaj: "Content",
	 dodaj_sliku: "Add Image",
	 back: "Back",
	 show: "Show",
	 izmeni_clanak: "Change Article",
	 view_all_articles: "View all articles",
	 dodaj_asistenciju: "Add Assistance",
	 body_of_comment: "Comment Body",
	 my_cms: "My CMS",
	 dodaj_zuti_karton: "Add yellow card",
	 zuti_karton: "Yellow card",
	 unesite_svoje_ime: "Enter your name",
	 naslov_poruke: "Title",
	 sadrzaj_poruke: "Content",
	 kontaktirajte_nas_putem_maila: "Contact us by mail",
	 lokacije_balona_u_kojima_se_igra: "Stadions locations that we play",
	 home_page: "Home Page",
	 enter_new_username: "Enter new Username",
	 enter_new_email: "Enter new Email",
	 enter_new_password: "Enter new Password",
	 posalji_poruku: "Send message",
	 svi_korisnici: "All Users",
	 ime_i_prezime: "Name and Surname",
	 moji_clanci: "My Articles",
	 suteva:"Shoots",
	 suteva_skraceno:"S",
	 unesi_ime_delegata:"Enter delegate Name",
	 unesi_prezime_delegata:"Enter delegate Surname",
	 unesi_lozinku_delegata:"Enter delegate Password",
	 unesi_registracioni_token:"Enter registration token",
	 delegate_edit:"Edit delegate",
	 delegate_update:"Update delegate",
	 all_delegates:"All delegates",
	 registruj_delegata:"Registrate a delegate",
	 mecevi_koji_treba_da_se_odigraju:"Matches to be played",
	 odigrani_mecevi:"finished matches",
	 molim_vas_sacekajte:"Please wait...",
	 dodaj_faul:"Add foul",
	 dodaj_odbranu:"Add defense",
	 dodaj_gol:"add goal",
	 lige:"League",
	 sve_lige:"All leagues",     
	 kreiraj_ligu:"Create league",
	 dodaj_gostujuce_igrace:"Add away players",
	 dodaj_domace_igrace:"Add home players",
	 odbrana:"Defense",
	 odbrana_skraceno:"Def",
	 delegat:"Delegate",
	 sudija:"Referee",
	 startuj_mec:"Start match",
	 startuj_mec_potvrda:"Start match - confirm",
	 kraj_I_poluvremena:"Half-time",
	 kraj_I_poluvremena_potvrda:"Half-time confirm",
	 pocetak_II_poluvremena:"Start second-half",
	 pocetak_II_poluvremena_potvrda:"Start second-half confirm",
	 kraj_meca:"End match",
	 kraj_meca_potvrda:"End match - confirm",
	 procenat_pobeda:"Won ratio",
	 procenat_neresenih:"Drew ratio",
	 procenat_izgubljenih:"Lost ratio",
	 faulova:"Fouls",
	 faulova_skraceno:"F",
	 level:"Level",
	 experience:"Experience",
	 experience_skraceno:"XP",
	 level_skraceno:"Lvl",
	 dodaj_crveni_karton:"Add red card",
	 sudije:"Referees",       
	 slika_sudije:"Referee image",
	 sve_sezone:"All seasons",
	 kreiraj_sezonu:"Create season",
	 prijavi_se:"sign up",
	 slika_igraca:"Player image",
	 ime: "Name",
	 prezime: "Last Name"
	 },

###############
	# 2. jezik
	# Srpski
	###############
	{
	 dodaj_sut:"Dodaj šut",
	 dodaj_igrace:"Dodaj igrače",
	 predstojeci_mecevi:"Predstojeći mečevi",
	 izaberi:"Izaberi",
	 poruke:"Poruke",
	 player_stats:"Statistika igrača",
	 player_bedzevi:"Bedzevi igrača",
	 player_team:"Ekipa",
	 petnaest_odbrana_na_utakmici:"15 odbrana na utakmici",
	 broj_pobeda:"Broj pobeda",
	 broj_neresenih:"Broj nerešenih",
	 broj_izgubljenih:"Broj izgubljenih",
	 email:"email",
	 unesi_prezime_kapitena:"Unesi prezime kapitena",
	 izmeni_podatke_za_ekipu:"Izmeni podatke za ekipu",
	 podesavanje_igraca:"Podešavanje igrača",
	 unesi_ime_kapitena:"Unesi ime kapitena",
	 svi_igraci:"Svi igrači",
	 registruj_igraca:"Registruj igrača",
	 korisnicko_ime:"Korisničko ime",
	 datum_rodjenja:"Datum rođenja",
	 odigranih_meceva:"Odigranih mečeva",
	 asistencije:"Asistencije",
	 golovi:"Golovi",
	 faulovi:"Faulovi",
	 zuti_kartoni:"Žuti kartoni",
	 crveni_kartoni:"Crveni kartoni",
	 tim:"Tim",
	 unesi_naziv_sezone:"Unesi naziv sezone",
	 registracija_tima_sa_8_igraca:"Registracija ekipe sa 8 igrača",
	 registracija_tima_sa_10_igraca:"Registracija ekipe sa 10 igrača",
	 registracija_tima_sa_12_igraca:"Registracija ekipe sa 12 igrača",
	 registracija_tima_sa_14_igraca:"Registracija ekipe sa 14 igrača",
	 registracija_tima_sa_16_igraca:"Registracija ekipe sa 16 igrača",
	 sve_ekipe:"Sve ekipe",
	 ime_ekipe:"Ime ekipe",
	 unesi_ime_ekipe:"Unesi ime ekipe",
	 kapiten:"Kapiten",
	 unesi_ime_igraca:"Unesi ime igrača",
	 unesi_prezime_igraca:"Unesi prezime igrača",
	 unesi_email_igraca:"Unesi email igrača",
	 unesi_lozinku_igraca:"Unesi lozinku igrača",
	 registracioni_token:"Registracioni token",
	 team_stats:"Statistike ekipe",
	 team_badges:"Bedzevi ekipe",
	 igraci:"Igrači",
	 rang_lista:"Rang lista",
	 ukucaj_poruku:"Ukucaj poruku",
	 share:"Podeli",
	 your_conversation:"Konverzacija",
	 toggle_navigation:"Toggle navigation",
	 pratite_nas:"Pratite nas",
	 godina:"godina",
	 potvrdi:"Potvrdi",
	 azuriraj_nalog:"Ažuriraj nalog",
	 prepiska_sa:"Prepiska sa",
	 restore:"Povrati",
	 poruka:"Poruka",
	 are_you_sure:"Da li ste sigurni?",
	 primljene_poruke:"Primljene poruke",
	 poslate_poruke:"Poslate poruke",
	 kanta:"Kanta",
	 isprazni_kantu:"Isprazni kantu",
	 posalji:"Pošalji",
	 podesavanja_naloga: "Podešavanje naloga",
	 informacije_o_meni: "Info",
	 moj_profil: "Moj profil",
	 kalendar_takmicenja: "Kalendar takmičenja",
	 my_articles: "Moji članci",
	 log_out: "Odjavi se",
	 created: "Napravljen",
	 create_article: "Napravi članak",
	 edit_this_article: "Izmeni članak",
	 delete_this_article: "Izbriši članak",
	 delete_article_confirmation: "Da li ste sigurni da želite da izbrišete ovaj članak?",
	 naslov: "Naslov",
	 sadrzaj: "Sadržaj",
	 dodaj_sliku: "Dodaj sliku",
	 back: "Nazad",
	 show: "Prikaži",
	 izmeni_clanak: "Izmeni članak",
	 view_all_articles: "Vidi sve članke",
	 dodaj_asistenciju: "Dodaj asistenciju",
	 body_of_comment: "Komentar",
	 my_cms: "Moj CMS",
	 dodaj_zuti_karton: "Dodaj žuti karton",
	 zuti_karton: "Žuti karton",
	 unesite_svoje_ime: "Unesite svoje ime",
	 naslov_poruke: "Naslov poruke",
	 sadrzaj_poruke: "Sadržaj poruke",
	 kontaktirajte_nas_putem_maila: "Kontaktirajte nas putem email-a",
	 lokacije_balona_u_kojima_se_igra: "Lokacije balona",
	 home_page: "Početna strana",
	 enter_new_username: "Unesite novo ime",
	 enter_new_email: "Unesite novi email",
	 enter_new_password: "Unesite novu lozinku ",
	 posalji_poruku: "Pošalji poruku",
	 svi_korisnici: "Svi korisnici",
	 ime_i_prezime: "Ime i prezime",
	 moji_clanci: "Moji članci",
	 suteva:"Šuteva",
	 suteva_skraceno:"S",
	 unesi_ime_delegata:"Unesite ime delegata",
	 unesi_prezime_delegata:"Unesite prezime delegata",
	 unesi_lozinku_delegata:"Unesite lozinku delegata",
	 unesi_registracioni_token:"Unesite registracioni token",
	 delegate_edit:"Izmeni delegata",
	 delegate_update:"Ažuriraj delegata",
	 all_delegates:"Svi delegati",
	 registruj_delegata:"Registruj delegata",
	 mecevi_koji_treba_da_se_odigraju:"Mečevi koji treba da se odigraju",
	 odigrani_mecevi:"Odigrani mečevi",
	 molim_vas_sacekajte:"Molimo vas sacekajte...",
	 dodaj_faul:"Dodaj faul",
	 dodaj_odbranu:"Dodaj odbranu",
	 dodaj_gol:"Dodaj gol",
	 lige:"Lige",
	 sve_lige:"Sve lige",     
	 kreiraj_ligu:"Kreiraj ligu",
	 dodaj_gostujuce_igrace:"Dodaj gostujuće igrače",
	 dodaj_domace_igrace:"Dodaj domaće igrače",
	 odbrana:"Odbrana",
	 odbrana_skraceno:"Odb",
	 delegat:"Delegat",
	 sudija:"Sudija",
	 startuj_mec:"Započni meč",
	 startuj_mec_potvrda:"Započni meč - potvrda",
	 kraj_I_poluvremena:"Kraj prvog poluvremena",
	 kraj_I_poluvremena_potvrda:"Kraj prvog poluvremena - potvrda",
	 pocetak_II_poluvremena:"Početak drugog poluvremena",
	 pocetak_II_poluvremena_potvrda:"Početak drugog poluvremena - potvrda",
	 kraj_meca:"Kraj meča",
	 kraj_meca_potvrda:"Kraj meča - potvrda",
	 procenat_pobeda:"Procenat pobeda",
	 procenat_neresenih:"Procenat nerešenih",
	 procenat_izgubljenih:"Procenat izgubljenih",
	 faulova:"Faulova",
	 faulova_skraceno:"F",
	 level:"Level",
	 experience:"Experience",
	 experience_skraceno:"XP",
	 level_skraceno:"Lvl",
	 dodaj_crveni_karton:"Dodaj crveni karton",
	 sudije:"Sudije",       
	 slika_sudije:"Slika sudije",
	 sve_sezone:"Sve sezone",
	 kreiraj_sezonu:"Kreiraj sezonu",
	 prijavi_se:"Prijavi se",
	 slika_igraca:"Slika igrača",
	 ime: "Ime",
	 prezime: "Prezime"
	 }
])

#####################################################
# PLAYERBADGES LANGUAGES
#####################################################
PlayerbadgesLanguage.create!([
	###############
	# 1. jezik
	# Engleski
	###############
	{	
	prvi_gol:"First goal",
	prva_asistencija:"First assistance",
	gol_u_prva_2_min:"Goal in first 2 min",
	gol_iz_slobodnog:"Free-kick goal ",
	gol_iz_kornera:"Corner-kick goal",
	gol_glavom:"Header goal",
	gol_sa_svoje_polovine:"Goal from half field",
	tri_gola_na_utakmici:"Scored 3 goals in a game ",
	sest_golova_na_utakmici:"Scored 6 goals in a game",
	deset_golova_na_utakmici:"Scored 10 goals in a game",
	tri_asist_na_utakmici:"3 assistances in a game",
	sest_asist_na_utakmici:"6 assistances in a game",
	deset_asist_na_utakmici:"10 assistances in a game",
	tri_odbrane_na_utakmici:"3 defenses in a game",
	sest_odbrana_na_utakmici:"6 defenses in a game",
	deset_odbrana_na_utakmici:"10 defenses in a game",
	gol_asistencija:"Goal and assistance",
	pet_gol_asistencija:"5 Goal-assistances",
	tri_gola_na_tri_uzastopne:"Scored 3 goals in three consecutive matches",
	tri_gola_na_bilo_koje_tri:"Scored 3 goals in any three matches",
	tri_asist_na_tri_uzastopne:"3 assistances in three consecutive matches",
	tri_asist_na_bilo_koje_tri:"3 assistances in any three matches",
	tri_gola_na_osam_utakmica:"Scored 3 goals in eight matches",
	odigrao_10_utakmica:"10 matches played",
	po_jedna_liga_kup_prijatelj:"Played 1 league, cup and friendly match",
	po_tri_liga_kup_prijatelj:"Played 3 league, cup and friendly matches",
	po_jedan_gol_l_k_p:"Scored goal in each league, cup and friendly match",
	pet_prijateljskih:"5 friendly match played",
	mvp_na_sedam_utakmica:"MVP in 7 matches",
	mvp_kupa:"Cup MVP",
	odigrane_sve_ligaske:"All league matches played"
	},
	###############
	# 2. jezik
	# Srpski
	###############
	{	
	prvi_gol:"Prvi gol",
	prva_asistencija:"Prva asistencija",
	gol_u_prva_2_min:"Gol u prva 2 min",
	gol_iz_slobodnog:"Gol iz slobodnog udarca",
	gol_iz_kornera:"Gol iz kornera",
	gol_glavom:"Gol glavom",
	gol_sa_svoje_polovine:"Gol sa svoje polovine",
	tri_gola_na_utakmici:"3 gola na utakmici",
	sest_golova_na_utakmici:"6 golova na utakmici",
	deset_golova_na_utakmici:"10 golova na utakmici",
	tri_asist_na_utakmici:"3 asistencije na utakmici",
	sest_asist_na_utakmici:"6 asistencija na utakmici",
	deset_asist_na_utakmici:"10 asistencija na utakmici",
	tri_odbrane_na_utakmici:"3 odbrane na utakmici",
	sest_odbrana_na_utakmici:"6 odbrana na utakmici",
	deset_odbrana_na_utakmici:"10 odbrana na utakmici",
	gol_asistencija:"Gol i asistencija",
	pet_gol_asistencija:"5 Gol-asistencija",
	tri_gola_na_tri_uzastopne:"3 gola na tri uzastopne utakmice",
	tri_gola_na_bilo_koje_tri:"3 gola na bilo koje tri utakmice",
	tri_asist_na_tri_uzastopne:"3 asistencije na tri uzastopne utakmice",
	tri_asist_na_bilo_koje_tri:"3 asistencije na bilo koje tri utakmice",
	tri_gola_na_osam_utakmica:"3 gola na osam utakmica",
	odigrao_10_utakmica:"10 odigranih utakmica",
	po_jedna_liga_kup_prijatelj:"Po jedna ligaska, prijateljska i kup utakmica",
	po_tri_liga_kup_prijatelj:"Po tri ligaske, prijateljske i kup utakmice",
	po_jedan_gol_l_k_p:"Po jedan gol na ligaskoj, prijateljskoj i kup utakmici",
	pet_prijateljskih:"5 prijateljskih utakmica",
	mvp_na_sedam_utakmica:"MVP na 7 utakmica",
	mvp_kupa:"Kup MVP",
	odigrane_sve_ligaske:"Odigrane sve ligaske utakmice",
	},
	###############
	# 3. jezik
	# Nemacki
	###############
	{	
	prvi_gol:"First goal",
	prva_asistencija:"First assistance",
	gol_u_prva_2_min:"Goal in first 2 min",
	gol_iz_slobodnog:"Free-kick goal ",
	gol_iz_kornera:"Corner-kick goal",
	gol_glavom:"Header goal",
	gol_sa_svoje_polovine:"Goal from half field",
	tri_gola_na_utakmici:"Scored 3 goals in a game ",
	sest_golova_na_utakmici:"Scored 6 goals in a game",
	deset_golova_na_utakmici:"Scored 10 goals in a game",
	tri_asist_na_utakmici:"3 assistances in a game",
	sest_asist_na_utakmici:"6 assistances in a game",
	deset_asist_na_utakmici:"10 assistances in a game",
	tri_odbrane_na_utakmici:"3 defenses in a game",
	sest_odbrana_na_utakmici:"6 defenses in a game",
	deset_odbrana_na_utakmici:"10 defenses in a game",
	gol_asistencija:"Goal-assistance",
	pet_gol_asistencija:"5 Goal-assistances",
	tri_gola_na_tri_uzastopne:"Scored 3 goals in three consecutive matches",
	tri_gola_na_bilo_koje_tri:"Scored 3 goals in any three matches",
	tri_asist_na_tri_uzastopne:"3 assistances in three consecutive matches",
	tri_asist_na_bilo_koje_tri:"3 assistances in any three matches",
	tri_gola_na_osam_utakmica:"Scored 3 goals in eight matches",
	odigrao_10_utakmica:"10 matches played",
	po_jedna_liga_kup_prijatelj:"Played 1 league, cup and friendly match",
	po_tri_liga_kup_prijatelj:"Played 3 league, cup and friendly matches",
	po_jedan_gol_l_k_p:"Scored goal in each league, cup and friendly match",
	pet_prijateljskih:"5 friendly match played",
	mvp_na_sedam_utakmica:"MVP in 7 matches",
	mvp_kupa:"Cup MVP",
	odigrane_sve_ligaske:"All league matches played"
	},
	###############
	# 4. jezik
	# Spanski
	###############
	{	
	prvi_gol:"First goal",
	prva_asistencija:"First assistance",
	gol_u_prva_2_min:"Goal in first 2 min",
	gol_iz_slobodnog:"Free-kick goal ",
	gol_iz_kornera:"Corner-kick goal",
	gol_glavom:"Header goal",
	gol_sa_svoje_polovine:"Goal from half field",
	tri_gola_na_utakmici:"Scored 3 goals in a game ",
	sest_golova_na_utakmici:"Scored 6 goals in a game",
	deset_golova_na_utakmici:"Scored 10 goals in a game",
	tri_asist_na_utakmici:"3 assistances in a game",
	sest_asist_na_utakmici:"6 assistances in a game",
	deset_asist_na_utakmici:"10 assistances in a game",
	tri_odbrane_na_utakmici:"3 defenses in a game",
	sest_odbrana_na_utakmici:"6 defenses in a game",
	deset_odbrana_na_utakmici:"10 defenses in a game",
	gol_asistencija:"Goal-assistance",
	pet_gol_asistencija:"5 Goal-assistances",
	tri_gola_na_tri_uzastopne:"Scored 3 goals in three consecutive matches",
	tri_gola_na_bilo_koje_tri:"Scored 3 goals in any three matches",
	tri_asist_na_tri_uzastopne:"3 assistances in three consecutive matches",
	tri_asist_na_bilo_koje_tri:"3 assistances in any three matches",
	tri_gola_na_osam_utakmica:"Scored 3 goals in eight matches",
	odigrao_10_utakmica:"10 matches played",
	po_jedna_liga_kup_prijatelj:"Played 1 league, cup and friendly match",
	po_tri_liga_kup_prijatelj:"Played 3 league, cup and friendly matches",
	po_jedan_gol_l_k_p:"Scored goal in each league, cup and friendly match",
	pet_prijateljskih:"5 friendly match played",
	mvp_na_sedam_utakmica:"MVP in 7 matches",
	mvp_kupa:"Cup MVP",
	odigrane_sve_ligaske:"All league matches played"
	},
	###############
	# 5. jezik
	# Portugalski
	###############
	{	
	prvi_gol:"First goal",
	prva_asistencija:"First assistance",
	gol_u_prva_2_min:"Goal in first 2 min",
	gol_iz_slobodnog:"Free-kick goal ",
	gol_iz_kornera:"Corner-kick goal",
	gol_glavom:"Header goal",
	gol_sa_svoje_polovine:"Goal from half field",
	tri_gola_na_utakmici:"Scored 3 goals in a game ",
	sest_golova_na_utakmici:"Scored 6 goals in a game",
	deset_golova_na_utakmici:"Scored 10 goals in a game",
	tri_asist_na_utakmici:"3 assistances in a game",
	sest_asist_na_utakmici:"6 assistances in a game",
	deset_asist_na_utakmici:"10 assistances in a game",
	tri_odbrane_na_utakmici:"3 defenses in a game",
	sest_odbrana_na_utakmici:"6 defenses in a game",
	deset_odbrana_na_utakmici:"10 defenses in a game",
	gol_asistencija:"Goal-assistance",
	pet_gol_asistencija:"5 Goal-assistances",
	tri_gola_na_tri_uzastopne:"Scored 3 goals in three consecutive matches",
	tri_gola_na_bilo_koje_tri:"Scored 3 goals in any three matches",
	tri_asist_na_tri_uzastopne:"3 assistances in three consecutive matches",
	tri_asist_na_bilo_koje_tri:"3 assistances in any three matches",
	tri_gola_na_osam_utakmica:"Scored 3 goals in eight matches",
	odigrao_10_utakmica:"10 matches played",
	po_jedna_liga_kup_prijatelj:"Played 1 league, cup and friendly match",
	po_tri_liga_kup_prijatelj:"Played 3 league, cup and friendly matches",
	po_jedan_gol_l_k_p:"Scored goal in each league, cup and friendly match",
	pet_prijateljskih:"5 friendly match played",
	mvp_na_sedam_utakmica:"MVP in 7 matches",
	mvp_kupa:"Cup MVP",
	odigrane_sve_ligaske:"All league matches played"
	},
	###############
	# 6. jezik
	# Francuski
	###############
	{	
	prvi_gol:"First goal",
	prva_asistencija:"First assistance",
	gol_u_prva_2_min:"Goal in first 2 min",
	gol_iz_slobodnog:"Free-kick goal ",
	gol_iz_kornera:"Corner-kick goal",
	gol_glavom:"Header goal",
	gol_sa_svoje_polovine:"Goal from half field",
	tri_gola_na_utakmici:"Scored 3 goals in a game ",
	sest_golova_na_utakmici:"Scored 6 goals in a game",
	deset_golova_na_utakmici:"Scored 10 goals in a game",
	tri_asist_na_utakmici:"3 assistances in a game",
	sest_asist_na_utakmici:"6 assistances in a game",
	deset_asist_na_utakmici:"10 assistances in a game",
	tri_odbrane_na_utakmici:"3 defenses in a game",
	sest_odbrana_na_utakmici:"6 defenses in a game",
	deset_odbrana_na_utakmici:"10 defenses in a game",
	gol_asistencija:"Goal-assistance",
	pet_gol_asistencija:"5 Goal-assistances",
	tri_gola_na_tri_uzastopne:"Scored 3 goals in three consecutive matches",
	tri_gola_na_bilo_koje_tri:"Scored 3 goals in any three matches",
	tri_asist_na_tri_uzastopne:"3 assistances in three consecutive matches",
	tri_asist_na_bilo_koje_tri:"3 assistances in any three matches",
	tri_gola_na_osam_utakmica:"Scored 3 goals in eight matches",
	odigrao_10_utakmica:"10 matches played",
	po_jedna_liga_kup_prijatelj:"Played 1 league, cup and friendly match",
	po_tri_liga_kup_prijatelj:"Played 3 league, cup and friendly matches",
	po_jedan_gol_l_k_p:"Scored goal in each league, cup and friendly match",
	pet_prijateljskih:"5 friendly match played",
	mvp_na_sedam_utakmica:"MVP in 7 matches",
	mvp_kupa:"Cup MVP",
	odigrane_sve_ligaske:"All league matches played"
	},
	###############
	# 7. jezik
	# Ruski
	###############
	{	
	prvi_gol:"First goal",
	prva_asistencija:"First assistance",
	gol_u_prva_2_min:"Goal in first 2 min",
	gol_iz_slobodnog:"Free-kick goal ",
	gol_iz_kornera:"Corner-kick goal",
	gol_glavom:"Header goal",
	gol_sa_svoje_polovine:"Goal from half field",
	tri_gola_na_utakmici:"Scored 3 goals in a game ",
	sest_golova_na_utakmici:"Scored 6 goals in a game",
	deset_golova_na_utakmici:"Scored 10 goals in a game",
	tri_asist_na_utakmici:"3 assistances in a game",
	sest_asist_na_utakmici:"6 assistances in a game",
	deset_asist_na_utakmici:"10 assistances in a game",
	tri_odbrane_na_utakmici:"3 defenses in a game",
	sest_odbrana_na_utakmici:"6 defenses in a game",
	deset_odbrana_na_utakmici:"10 defenses in a game",
	gol_asistencija:"Goal-assistance",
	pet_gol_asistencija:"5 Goal-assistances",
	tri_gola_na_tri_uzastopne:"Scored 3 goals in three consecutive matches",
	tri_gola_na_bilo_koje_tri:"Scored 3 goals in any three matches",
	tri_asist_na_tri_uzastopne:"3 assistances in three consecutive matches",
	tri_asist_na_bilo_koje_tri:"3 assistances in any three matches",
	tri_gola_na_osam_utakmica:"Scored 3 goals in eight matches",
	odigrao_10_utakmica:"10 matches played",
	po_jedna_liga_kup_prijatelj:"Played 1 league, cup and friendly match",
	po_tri_liga_kup_prijatelj:"Played 3 league, cup and friendly matches",
	po_jedan_gol_l_k_p:"Scored goal in each league, cup and friendly match",
	pet_prijateljskih:"5 friendly match played",
	mvp_na_sedam_utakmica:"MVP in 7 matches",
	mvp_kupa:"Cup MVP",
	odigrane_sve_ligaske:"All league matches played"
	}
])
#####################################################
# TEAMBADGES LANGUAGES
#####################################################
TeambadgesLanguage.create!([
	###############
	# 1. jezik
	# Engleski
	###############
	{
	prva_pobeda:"First win",
	tri_uzastopne_pobede:"3 games won in a row",
	sedam_uzastopnih_pobeda:"7 games won in a row",
	sve_pobede_u_ligi:"All games won in league",
	osvojena_liga:"Won league",
	osvojen_kup:"Won cup",
	pet_prijateljskih_odirano:"5 friendly matches played",
	tri_bez_faula_i_kartona:"3 matches without fouls and cards",
	tri_vezane_bez_primljenog:"3 matches in a row without recived goal",
	u_osam_kola_mvp:"MVP in 8 matches",
	pobeda_sa_10_golova_razlike:"Won with the 10-goal difference",
	dato_vise_od_10_golova:"Scored more than 10 goals",
	dato_vise_od_20_golova:"Scored more than 20 goals",
	primljeno_0_golova:"0 goals recived",
	bez_faula_i_kartona:"Match without fouls and cards",
	iz_2_u_1:"Home team lost the first half but won the match",
	sutevi_golovi_sto_posto:"100%\ efficiently",
	tri_gola_tri_igraca:"3 players have each scored 3 goals",
	sedam_plus_asistencija:"7+ assistances",
	svi_igraci_dali_gol:"All players scored a goal"
	},
	###############
	# 2. jezik
	# Srpski
	###############
	{
	prva_pobeda:"Prva pobeda",
	tri_uzastopne_pobede:"3 uzastopne pobede",
	sedam_uzastopnih_pobeda:"7 uzastopnih pobeda",
	sve_pobede_u_ligi:"Sve pobede u ligi",
	osvojena_liga:"Osvojena liga",
	osvojen_kup:"Osvojen Kup",
	pet_prijateljskih_odirano:"Odigrano 5 prijateljskih utakmica",
	tri_bez_faula_i_kartona:"3 utakmice bez faula i kartona",
	tri_vezane_bez_primljenog:"3 vezane utakmice bez primljenog gola",
	u_osam_kola_mvp:"MVP u 8 kola",
	pobeda_sa_10_golova_razlike:"Pobeda sa 10 golova razlike",
	dato_vise_od_10_golova:"Dato vise od 10 golova",
	dato_vise_od_20_golova:"Dato vise od 20 golova",
	primljeno_0_golova:"Primljeno 0 golova",
	bez_faula_i_kartona:"Utakmica bez faula i kartona",
	iz_2_u_1:"Iz 2 u 1",
	sutevi_golovi_sto_posto:"100%\ efikasnost",
	tri_gola_tri_igraca:"3 igraca postiglo po 3 gola",
	sedam_plus_asistencija:"7+ asistencija",
	svi_igraci_dali_gol:"Svi igraci postigli gol"
	},
	###############
	# 3. jezik
	# Nemacki
	###############
	{
	prva_pobeda:"First win",
	tri_uzastopne_pobede:"3 games won in a row",
	sedam_uzastopnih_pobeda:"7 games won in a row",
	sve_pobede_u_ligi:"All games won in league",
	osvojena_liga:"Won league",
	osvojen_kup:"Won cup",
	pet_prijateljskih_odirano:"5 friendly matches played",
	tri_bez_faula_i_kartona:"3 matches without fouls and cards",
	tri_vezane_bez_primljenog:"3 matches in a row without recived goal",
	u_osam_kola_mvp:"MVP in 8 matches",
	pobeda_sa_10_golova_razlike:"Won with the 10-goal difference",
	dato_vise_od_10_golova:"Scored more than 10 goals",
	dato_vise_od_20_golova:"Scored more than 20 goals",
	primljeno_0_golova:"0 goals recived",
	bez_faula_i_kartona:"Match without fouls and cards",
	iz_2_u_1:"Home team lost the first half but won the match",
	sutevi_golovi_sto_posto:"100%\ efficiently",
	tri_gola_tri_igraca:"3 players have each scored 3 goals",
	sedam_plus_asistencija:"7+ assistances",
	svi_igraci_dali_gol:"All players scored a goal"
	},
	###############
	# 4. jezik
	# Spanski
	###############
	{
	prva_pobeda:"First win",
	tri_uzastopne_pobede:"3 games won in a row",
	sedam_uzastopnih_pobeda:"7 games won in a row",
	sve_pobede_u_ligi:"All games won in league",
	osvojena_liga:"Won league",
	osvojen_kup:"Won cup",
	pet_prijateljskih_odirano:"5 friendly matches played",
	tri_bez_faula_i_kartona:"3 matches without fouls and cards",
	tri_vezane_bez_primljenog:"3 matches in a row without recived goal",
	u_osam_kola_mvp:"MVP in 8 matches",
	pobeda_sa_10_golova_razlike:"Won with the 10-goal difference",
	dato_vise_od_10_golova:"Scored more than 10 goals",
	dato_vise_od_20_golova:"Scored more than 20 goals",
	primljeno_0_golova:"0 goals recived",
	bez_faula_i_kartona:"Match without fouls and cards",
	iz_2_u_1:"Home team lost the first half but won the match",
	sutevi_golovi_sto_posto:"100%\ efficiently",
	tri_gola_tri_igraca:"3 players have each scored 3 goals",
	sedam_plus_asistencija:"7+ assistances",
	svi_igraci_dali_gol:"All players scored a goal"
	},
	###############
	# 5. jezik
	# Portugalski
	###############
	{
	prva_pobeda:"First win",
	tri_uzastopne_pobede:"3 games won in a row",
	sedam_uzastopnih_pobeda:"7 games won in a row",
	sve_pobede_u_ligi:"All games won in league",
	osvojena_liga:"Won league",
	osvojen_kup:"Won cup",
	pet_prijateljskih_odirano:"5 friendly matches played",
	tri_bez_faula_i_kartona:"3 matches without fouls and cards",
	tri_vezane_bez_primljenog:"3 matches in a row without recived goal",
	u_osam_kola_mvp:"MVP in 8 matches",
	pobeda_sa_10_golova_razlike:"Won with the 10-goal difference",
	dato_vise_od_10_golova:"Scored more than 10 goals",
	dato_vise_od_20_golova:"Scored more than 20 goals",
	primljeno_0_golova:"0 goals recived",
	bez_faula_i_kartona:"Match without fouls and cards",
	iz_2_u_1:"Home team lost the first half but won the match",
	sutevi_golovi_sto_posto:"100%\ efficiently",
	tri_gola_tri_igraca:"3 players have each scored 3 goals",
	sedam_plus_asistencija:"7+ assistances",
	svi_igraci_dali_gol:"All players scored a goal"
	},
	###############
	# 6. jezik
	# Francuski
	###############
	{
	prva_pobeda:"First win",
	tri_uzastopne_pobede:"3 games won in a row",
	sedam_uzastopnih_pobeda:"7 games won in a row",
	sve_pobede_u_ligi:"All games won in league",
	osvojena_liga:"Won league",
	osvojen_kup:"Won cup",
	pet_prijateljskih_odirano:"5 friendly matches played",
	tri_bez_faula_i_kartona:"3 matches without fouls and cards",
	tri_vezane_bez_primljenog:"3 matches in a row without recived goal",
	u_osam_kola_mvp:"MVP in 8 matches",
	pobeda_sa_10_golova_razlike:"Won with the 10-goal difference",
	dato_vise_od_10_golova:"Scored more than 10 goals",
	dato_vise_od_20_golova:"Scored more than 20 goals",
	primljeno_0_golova:"0 goals recived",
	bez_faula_i_kartona:"Match without fouls and cards",
	iz_2_u_1:"Home team lost the first half but won the match",
	sutevi_golovi_sto_posto:"100%\ efficiently",
	tri_gola_tri_igraca:"3 players have each scored 3 goals",
	sedam_plus_asistencija:"7+ assistances",
	svi_igraci_dali_gol:"All players scored a goal"
	},
	###############
	# 7. jezik
	# Ruski
	###############
	{
	prva_pobeda:"First win",
	tri_uzastopne_pobede:"3 games won in a row",
	sedam_uzastopnih_pobeda:"7 games won in a row",
	sve_pobede_u_ligi:"All games won in league",
	osvojena_liga:"Won league",
	osvojen_kup:"Won cup",
	pet_prijateljskih_odirano:"5 friendly matches played",
	tri_bez_faula_i_kartona:"3 matches without fouls and cards",
	tri_vezane_bez_primljenog:"3 matches in a row without recived goal",
	u_osam_kola_mvp:"MVP in 8 matches",
	pobeda_sa_10_golova_razlike:"Won with the 10-goal difference",
	dato_vise_od_10_golova:"Scored more than 10 goals",
	dato_vise_od_20_golova:"Scored more than 20 goals",
	primljeno_0_golova:"0 goals recived",
	bez_faula_i_kartona:"Match without fouls and cards",
	iz_2_u_1:"Home team lost the first half but won the match",
	sutevi_golovi_sto_posto:"100%\ efficiently",
	tri_gola_tri_igraca:"3 players have each scored 3 goals",
	sedam_plus_asistencija:"7+ assistances",
	svi_igraci_dali_gol:"All players scored a goal"
	}
])



#####################################################
# ORGANISATION INFORMATIONS
#####################################################
OrganisationInformation.create!([
	###############
	# 1. jezik
	# English
	###############
	{
	 organisation_name: "Your organisation name",
	 moto: "Futsal is your passion? You enjoy competing? [You can change this text in CMS]",
	 header_above_registration_link: "Futsal is your passion? You enjoy competing? [You can change this text in CMS]",
	 about_your_organisation_above_registration_link: "Than GTC sport is right place for you. If you have a team with which you play constantly,
log on to our website and play quality football after the FIFA regulations, once a week, each time against the other team. Formed with the objective to promote amateur sport and socializing.",
	 box_1: "Formed with the objective to promote amateur sport and socializing",
	 box_2: "Respect the rules of fair play and respect your opponents!",
	 box_3: "We play in the best indoor fields in the city and on weekends in the best time.",
	 box_4: "We have provided cash and merchandise prizes and also trophies for the best teams and individuals.",
	 box_5: "We have provided the best group of arbitrators with extensive experience in trial tournaments and indoor soccer leagues, which are there to provide participants a fair and proper trial.",
	 box_6: "In case you do not have a team, and would like to play, sign up- we’ll find a place for you!",
	 number_of_teams: "120",
	 number_of_players: "1859",
	 number_of_stadions: "15", 
	 number_of_leagues: "11",
	 facebook_link: "#",
	 twitter_link: "#",
	 pinterest_link: "#",
	 instagram_link: "#",
	 email_1: "office@gtcsport.com",
	 email_2: "support@gtcsport.com",
	 email_3: "",
	 email_4: "",
	 phone_1: "your phone 1",
	 phone_2: "your phone 2"
	 },
	###############
	# 2. jezik
	# Srpski
	###############
	{
	 organisation_name: "Ime Vaše organizacije",
	 moto: "Moto Vaše organizacije",
	 header_above_registration_link: "Futsal is your passion? You enjoy competing? [You can change this text in CMS]",
	 about_your_organisation_above_registration_link: "Than GTC sport is right place for you. If you have a team with which you play constantly,
log on to our website and play quality football after the FIFA regulations, once a week, each time against the other team. Formed with the objective to promote amateur sport and socializing.",
	 box_1: "Formed with the objective to promote amateur sport and socializing",
	 box_2: "Respect the rules of fair play and respect your opponents!",
	 box_3: "We play in the best indoor fields in the city and on weekends in the best time.",
	 box_4: "We have provided cash and merchandise prizes and also trophies for the best teams and individuals.",
	 box_5: "We have provided the best group of arbitrators with extensive experience in trial tournaments and indoor soccer leagues, which are there to provide participants a fair and proper trial.",
	 box_6: "In case you do not have a team, and would like to play, sign up- we’ll find a place for you!",
	 number_of_teams: "120",
	 number_of_players: "1859",
	 number_of_stadions: "15", 
	 number_of_leagues: "11",
	 facebook_link: "#",
	 twitter_link: "#",
	 pinterest_link: "#",
	 instagram_link: "#",
	 email_1: "office@gtcsport.com",
	 email_2: "support@gtcsport.com",
	 email_3: "",
	 email_4: "",
	 phone_1: "your phone 1",
	 phone_2: "your phone 2"
	 },

])
#####################################################
# TEAM_SEASONS
#####################################################
#TeamSeason.create!([
	# Ekipe Prva liga 5+1
	#{team_id: 1, season_id: 1 },
	#{team_id: 2, season_id: 1 },
	#{team_id: 3, season_id: 1 },
	#{team_id: 4, season_id: 1 },
	#{team_id: 5, season_id: 1 },
	#{team_id: 6, season_id: 1 },
	#{team_id: 7, season_id: 1 }
	# Ekipe Druga liga 5+1
	#{team_id: 8 },
	#{team_id: 9 },
	#{team_id: 10 },
	# Ekipe Treca liga 5+1
	#{team_id: 11 },
	#{team_id: 12 },
	#{team_id: 13 },
#])

#####################################################
# TEAM_BADGES
#####################################################
#TeamBadge.create!([
	# Ekipe Prva liga 5+1
	#{team_id: 1 },
	#{team_id: 2 },
	#{team_id: 3 },
	#{team_id: 4 },
	#{team_id: 5 },
	#{team_id: 6 },
	#{team_id: 7 }
	# Ekipe Druga liga 5+1
	#{team_id: 8 },
	#{team_id: 9 },
	#{team_id: 10 },
	# Ekipe Treca liga 5+1
	#{team_id: 11 },
	#{team_id: 12 },
	#{team_id: 13 },
#])

#####################################################
# PLAYERS
#####################################################
# Inace se automatski prave PLAYER SEASONS i
# PLAYER BADGES, medjutim, ovde moramo sami da
# pravimo
#####################################################
@brojac_igraca = 0
85.times do |counter| 
	Player.create!([
		{name: "player-#{@brojac_igraca+1}", surname: "gtcsport", JMBG: "345216#{@brojac_igraca+1}", team_id: "#{counter+1}" },
		{name: "player-#{@brojac_igraca+2}", surname: "gtcsport", JMBG: "345216#{@brojac_igraca+2}", team_id: "#{counter+1}" },
		{name: "player-#{@brojac_igraca+3}", surname: "gtcsport", JMBG: "345216#{@brojac_igraca+3}", team_id: "#{counter+1}" },
		{name: "player-#{@brojac_igraca+4}", surname: "gtcsport", JMBG: "345216#{@brojac_igraca+4}", team_id: "#{counter+1}" },
		{name: "player-#{@brojac_igraca+5}", surname: "gtcsport", JMBG: "345216#{@brojac_igraca+5}", team_id: "#{counter+1}" },
		{name: "player-#{@brojac_igraca+6}", surname: "gtcsport", JMBG: "345216#{@brojac_igraca+6}", team_id: "#{counter+1}" },
		{name: "player-#{@brojac_igraca+7}", surname: "gtcsport", JMBG: "345216#{@brojac_igraca+7}", team_id: "#{counter+1}" },
		{name: "player-#{@brojac_igraca+8}", surname: "gtcsport", JMBG: "345216#{@brojac_igraca+8}", team_id: "#{counter+1}" }
	])
	@brojac_igraca += 8
end

#####################################################
# PLAYER_SEASONS
#####################################################
#PlayerSeason.create!([
	# Ekipe Prva liga 5+1
	#{player_id: 1, season_id: 1 },
	#{player_id: 2, season_id: 1 },
	#{player_id: 3, season_id: 1 },
	#{player_id: 4, season_id: 1 },
	#{player_id: 5, season_id: 1 },
	#{player_id: 6, season_id: 1 },
	#{player_id: 7, season_id: 1 },
	#{player_id: 8, season_id: 1 },
	#{player_id: 9, season_id: 1 },
	#{player_id: 10, season_id: 1 },
	#{player_id: 11, season_id: 1 },
	#player_id: 12, season_id: 1 },
	#{player_id: 13, season_id: 1 },
	#{player_id: 14, season_id: 1 },
	#{player_id: 15, season_id: 1 },
	#{player_id: 16, season_id: 1 },
	#{player_id: 17, season_id: 1 },
	#{player_id: 18, season_id: 1 },
	#{player_id: 19, season_id: 1 },
	#{player_id: 20, season_id: 1 },
	#{player_id: 21, season_id: 1 },
	#{player_id: 22, season_id: 1 },
	#{player_id: 23, season_id: 1 },
	#{player_id: 24, season_id: 1 },
	#{player_id: 25, season_id: 1 },
	#{player_id: 26, season_id: 1 },
	#{player_id: 27, season_id: 1 },
	#{player_id: 28, season_id: 1 },
	#{player_id: 29, season_id: 1 },
	#{player_id: 30, season_id: 1 },
	#{player_id: 31, season_id: 1 },
	#{player_id: 32, season_id: 1 },
	#{player_id: 33, season_id: 1 },
	#{player_id: 34, season_id: 1 },
	#{player_id: 35, season_id: 1 },
	#{player_id: 36, season_id: 1 },
	#{player_id: 37, season_id: 1 },
	#{player_id: 38, season_id: 1 },
	#{player_id: 39, season_id: 1 },
	#{player_id: 40, season_id: 1 },
	#{player_id: 41, season_id: 1 },
	#{player_id: 42, season_id: 1 },
	#{player_id: 43, season_id: 1 },
	#{player_id: 44, season_id: 1 },
	#{player_id: 45, season_id: 1 },
	#{player_id: 46, season_id: 1 },
	#{player_id: 47, season_id: 1 },
	#{player_id: 48, season_id: 1 },
	#{player_id: 49, season_id: 1 },
	#{player_id: 50, season_id: 1 },
	#{player_id: 51, season_id: 1 },
	#{player_id: 52, season_id: 1 },
	#{player_id: 53, season_id: 1 },
	#{player_id: 54, season_id: 1 },
	#{player_id: 55, season_id: 1 },
	#{player_id: 56, season_id: 1 }
#])

#####################################################
# PLAYER_BADGES
#####################################################
#PlayerBadge.create!([
	# Ekipe Prva liga 5+1
	#{player_id: 1 },
	#{player_id: 2 },
	#{player_id: 3 },
	#{player_id: 4 },
	#{player_id: 5 },
	#{player_id: 6 },
	#{player_id: 7 },
	#{player_id: 8 },
	#{player_id: 9 },
	#{player_id: 10 },
	#{player_id: 11 },
	#{player_id: 12 },
	#{player_id: 13 },
	#{player_id: 14 },
	#{player_id: 15 },
	#{player_id: 16 },
	#player_id: 17 },
	#{player_id: 18 },
	#{player_id: 19 },
	#{player_id: 20 },
	#{player_id: 21 },
	#{player_id: 22 },
	#{player_id: 23 },
	#{player_id: 24 },
	#{player_id: 25 },
	#{player_id: 26 },
	#{player_id: 27 },
	#{player_id: 28 },
	#{player_id: 29 },
	#{player_id: 30 },
	#{player_id: 31 },
	#{player_id: 32 },
	#{player_id: 33 },
	#{player_id: 34 },
	#{player_id: 35 },
	#{player_id: 36 },
	#{player_id: 37 },
	#{player_id: 38 },
	#{player_id: 39 },
	#{player_id: 40 },
	#{player_id: 41 },
	#{player_id: 42 },
	#{player_id: 43 },
	#{player_id: 44 },
	#{player_id: 45 },
	#{player_id: 46 },
	#{player_id: 47 },
	#{player_id: 48 },
	#{player_id: 49 },
	#{player_id: 50 },
	#{player_id: 51 },
	#{player_id: 52 },
	#{player_id: 53 },
	#{player_id: 54 },
	#{player_id: 55 },
	#{player_id: 56 }
#])

#####################################################
# DELEGATES
#####################################################
Delegate.create!([
	# Barselona
	{name: "delegate1", surname: "gtcsport", JMBG: "3232163416" },
	{name: "delegate2", surname: "gtcsport", JMBG: "33252163426" }
])

#####################################################
# MATCHES
#####################################################
10.times do |counter| 
	Match.create!([
	  {home_team_id: "#{counter+1}", away_team_id: "#{counter+3}",
	   delegate_id: "1", referee_id: "1", stadion_id: "1",
	   is_first_league: "1", league_id: "1", season_id: "1",
	 	 match_date: DateTime.parse("09/01/2016 17:00")}
	])
end

10.times do |counter| 
	Match.create!([
	  {home_team_id: "#{counter+5}", away_team_id: "#{counter+7}", 
	   delegate_id: "1", referee_id: "1", stadion_id: "1",
	 	 is_first_league: "1", league_id: "2", season_id: "1",
	 	 match_date: DateTime.parse("09/01/2016 17:00")}
	])
end

10.times do |counter| 
	Match.create!([
	  {home_team_id: "#{counter+5}", away_team_id: "#{counter+7}", 
	   delegate_id: "1", referee_id: "1", stadion_id: "1",
	 	 is_first_league: "1", league_id: "3", season_id: "1",
	 	 match_date: DateTime.parse("09/01/2016 17:00")}
	])
end

#####################################################
# USERS
#####################################################
User.create!([
	# Admin users
	{email: "admin1@gtcsport.com", password: "gtcsport", admin_id: 1 },
	{email: "admin2@gtcsport.com", password: "gtcsport", admin_id: 2 },
	
	# Delegate users
	{email: "delegate1@gtcsport.com", password: "gtcsport", delegate_id: 1 },
	{email: "delegate2@gtcsport.com", password: "gtcsport", delegate_id: 2 }
])

@brojac_igraca = 0
85.times do |counter|
	User.create!([
		{email: "player#{@brojac_igraca+1}@gtcsport.com", password: "gtcsport", player_id: "#{@brojac_igraca+1}" },
		{email: "player#{@brojac_igraca+2}@gtcsport.com", password: "gtcsport", player_id: "#{@brojac_igraca+2}" },
		{email: "player#{@brojac_igraca+3}@gtcsport.com", password: "gtcsport", player_id: "#{@brojac_igraca+3}" },
		{email: "player#{@brojac_igraca+4}@gtcsport.com", password: "gtcsport", player_id: "#{@brojac_igraca+4}" },
		{email: "player#{@brojac_igraca+5}@gtcsport.com", password: "gtcsport", player_id: "#{@brojac_igraca+5}" },
		{email: "player#{@brojac_igraca+6}@gtcsport.com", password: "gtcsport", player_id: "#{@brojac_igraca+6}" },
		{email: "player#{@brojac_igraca+7}@gtcsport.com", password: "gtcsport", player_id: "#{@brojac_igraca+7}" },
		{email: "player#{@brojac_igraca+8}@gtcsport.com", password: "gtcsport", player_id: "#{@brojac_igraca+8}" }
	])
	@brojac_igraca += 8
end