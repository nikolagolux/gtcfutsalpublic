#####################################################
# ADMINS
#####################################################
Admin.create!([
	# Barselona
	{name: "Vukašin", surname: "Simić" },
	{name: "Nikola",  surname: "Pačić" },
	{name: "golux",	  surname: "golux" }
])

#####################################################
# User
#####################################################
User.create!([
	# Admin users
	{email: "vukasin.simic91@gmail.com", password: "organizator91", admin_id: 1 },
	{email: "nikolapacic@gmail.com", 	 password: "organizator91", admin_id: 2 },
	{email: "petar.sijan@goluxtech.com", password: "golux333", 		admin_id: 3 }
])

#####################################################
# BALF INFORMATION
#####################################################
OrganisationInformation.create!([
	{number_of_teams: "0", number_of_players: "0", number_of_stadions: "0", number_of_leagues: "0"}
])